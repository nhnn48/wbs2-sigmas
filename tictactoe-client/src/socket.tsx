import { io, Socket } from 'socket.io-client';

const socket: Socket = io(window.location.protocol + '//' + window.location.host, {
    autoConnect: true
});

const fetchSessionUser = async () => {
    try {
        const response = await fetch('/session/getSessionUser', { method: 'GET' });
        if (!response.ok) {
            throw new Error('Failed to fetch session user ID');
        }
        return await response.json();
    } catch (error) {
        console.error('Error fetching session user ID:', error);
    }
};

//save USerID in Socket for GameLogic
socket.on('connect', async () => {
    const userId = await fetchSessionUser();
    socket.emit('authenticate', userId);
});

export default socket;
