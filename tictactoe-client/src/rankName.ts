export class RankName{

    public static getRankName(elo: number): string{
        if (elo < 150) return 'Toesucker';
        else if (elo < 300) return 'L';
        else if (elo < 450) return 'Whack';
        else if (elo < 600) return 'WienerSchwiener';
        else if (elo < 750) return 'baka';
        else if (elo < 900) return 'CTier';
        else if (elo < 1050) return 'OrdinärerGamer';
        else if (elo < 1200) return 'KrasserGamer';
        else if (elo < 1350) return 'EpicGamer';
        else if (elo < 1500) return 'Toemaster';
        else if (elo < 1650) return 'ZehbraZähmer';
        else if (elo < 1800) return 'TicTacTitan';
        else if (elo < 1950) return 'ProfiTicker';
        else if (elo < 2100) return 'AlbusToeSenpai';
        else if (elo > 2250) return 'xxSuperSwaggerXX';
        return '';
    }

    public static getElo(elo: string): number{
        if (elo === 'Toesucker') return 0;
        else if (elo === 'L') return 150;
        else if (elo === 'Whack') return 300;
        else if (elo === 'WienerSchwiener') return 450;
        else if (elo === 'baka') return 600;
        else if (elo === 'CTier') return 750;
        else if (elo === 'OrdinärerGamer') return 900;
        else if (elo === 'KrasserGamer') return 1050;
        else if (elo === 'EpicGamer') return 1200;
        else if (elo === 'Toemaster') return 1350;
        else if (elo === 'ZehbraZähmer') return 1500;
        else if (elo === 'TicTacTitan') return 1650;
        else if (elo === 'ProfiTicker') return 1800;
        else if (elo === 'AlbusToeSenpai') return 1950;
        else if (elo === 'xxSuperSwaggerXX') return 2100;
        return 0;
    }
}