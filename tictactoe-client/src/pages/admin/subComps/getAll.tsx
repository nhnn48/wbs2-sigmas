import {useEffect, useState} from "react";
import Col from "react-bootstrap/Col";
import Container from "react-bootstrap/Container";
import Image from "react-bootstrap/Image";
import "./getAll.css"
import Row from "react-bootstrap/Row";


export default function GetAll() {
    const [data, setData] = useState<any[]>([]);

    // Holt alle nutzer und setzt den Zustand der Komponente den empfangenen daten
    useEffect(() => {
        fetch("/user/all", {method: "GET"})
            .then(response => {
                return response.json();
            }).then(async data => {
            setData(data)
        }).catch(error => {
            console.error('Error fetching users:', error);
        });
    }, []);


    return (
        <>
            <Container>
                <Col>
                            {data.map((data) => (

                                <Row key={data.id} className="border border-dark border-2 rounded-2 mt-2">
                                    <Col sm={1}>
                                        <h3 className="text-dark">{data.id}</h3>
                                    </Col>
                                    <Col sm={3}>
                                        <h3 className="text-dark">{data.username}</h3>
                                    </Col>
                                    <Col sm={2}>
                                        <h3 className="text-dark">{data.firstname}</h3>
                                    </Col>
                                    <Col sm={2}>
                                        <h3 className="text-dark">{data.lastname}</h3>
                                    </Col>
                                    <Col sm={2}>
                                        <h3 className="text-dark">{data.elo}</h3>
                                    </Col>
                                    <Col sm={2}>
                                        <Image
                                            src={`${window.location.protocol}//${window.location.host}/user/pfp/${data.profilePic}`}
                                            roundedCircle className="profile"/>
                                    </Col>
                                </Row>

                            ))}

                </Col>
            </Container>
        </>
    )
}