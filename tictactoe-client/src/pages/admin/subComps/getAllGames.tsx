import {useEffect, useState} from "react";
import Col from "react-bootstrap/Col";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Image from "react-bootstrap/Image";
import "./getAllGames.css"
import socket from "../../../socket.tsx";
import {fetchSessionRole} from "../../../service/sessionService.ts";


export default function GetAllGames() {
    const [data, setData] = useState<any[]>([]);
    const [currentRole, setRole] = useState<string>();

    // Gets the role of the session and updates the state of the component if the role is admin
    useEffect(() => {
        fetchSessionRole().then((role)=> {
            if (role == 'admin') {
                setRole(role);
            }
            return;
        })
    }, []);

        // Fetches all games that are unfinished, sets the state of the component equal to the received data
        const getGames = () => {
        fetch("/game/unfinishedAll", {method: "GET"})
            .then(response => response.json())
            .then((data) => {
            setData(data)
        })
            .catch(error => console.error('Error fetching games:', error)); }

        // Subscribes to the socket event 'gameUpdate', compares whether the role is admin and if so, calls getGames()
        socket.on('gameUpdate',() => {
                if (currentRole === "admin") {
                    getGames()
                }
        });

    useEffect(() => {
        getGames();
    }, []);


    return (
        <>
            <Container>
                <Col>
                    {data.map((data) => (
                        <div key={data.id}>
                                <Row className="align-items-center border border-dark border-2 m-5 pt-4 pb-4 ps-5 pe-5">
                                    <Col sm={2} className="p-0">
                                        <Container className="text-center p-0">
                                            <Image
                                                src={`${window.location.protocol}//${window.location.host}/user/pfp/${data.player1.profilePic}`}
                                                roundedCircle className="profileImage"/>
                                        </Container>
                                    </Col>
                                    <Col sm={2}>
                                        <p className="line">{data.player1.username}</p>
                                        <p>{data.player1.elo}</p>
                                    </Col>
                                    <Col sm={1}>
                                        <h2 className="text-center cross">X</h2>
                                    </Col>
                                    <Col sm={2}>
                                        <h2 className="text-center text-dark">-</h2>
                                    </Col>
                                    <Col sm={1}>
                                        <h2 className="text-center circle">O</h2>
                                    </Col>
                                    <Col sm={2}>
                                        <p className="line">{data.player2.username}</p>
                                        <p>{data.player2.elo}</p>
                                    </Col>
                                    <Col sm={2} className="p-0">
                                        <Container className="p-0 text-center">
                                        <Image
                                            src={`${window.location.protocol}//${window.location.host}/user/pfp/${data.player2.profilePic}`}
                                            roundedCircle className="profileImage" />
                                        </Container>
                                    </Col>
                                </Row>
                        </div>
                    ))}
                </Col>
            </Container>
        </>
    )
}