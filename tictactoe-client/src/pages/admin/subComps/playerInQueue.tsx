import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import {useEffect, useState} from "react";
import socket from '../../../socket.tsx';

interface User {
    id: number;
    username: string;
    elo: number
}

export default function PlayerInQueue() {

    const [queue, setQueue] = useState<Map<number, User> | null>(null);


    useEffect(() => {
        // Converts the updated queue array into a map and updates the state in the component
        const handleQueueUpdate = (updatedQueue: User[]) => {
            const mapQueue: Map<number, User> = new Map(updatedQueue.map(user => [user.id, user]));
            setQueue(mapQueue);
        };

        socket.on('queue', handleQueueUpdate);
        socket.on('getQueue', updatedQueue => {
            const mapQueue: Map<number, User> = new Map(updatedQueue);
            setQueue(mapQueue);
        });

        // Asks for current queue
        socket.emit('getQueue');

    }, []);


    if (!queue || queue.size === 0) {
        return <div>Queue is empty</div>;
    }

    return (
        <>
            <Row>
                <Col>
                    {[...queue.values()].map(user => (
                        <Container key={user.id} className="mt-4 mb-3 border border-dark border-2 rounded-2 p-2">
                            <Row>
                                <Col className="text-start">
                                    <span>User: {user.username}</span>
                                </Col>
                                <Col className="text-end">
                                    <span>Elo: {user.elo}</span>
                                </Col>
                            </Row>
                        </Container>
                    ))}

                </Col>
            </Row>
        </>
    )
}