import "./adminPage.css";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import {useState} from "react";
import UserOverview from "./userOverview.tsx";
import RunningGames from "./runningGames.tsx";
import Container from "react-bootstrap/Container";
import MatchmakingQuwu from "./matchmakingQuwu.tsx";


export default function AdminPage() {

    const [showUserOverview, setShowUserOverview] = useState(false);
    const [showQueue, setShowQueue] = useState(false);
    const [showCurrentGames, setShowCurrentGames] = useState(true);

    // Shows the respective page section depending on the clicked heading and hides the others
    const handleUserOverviewClick = () => {
        setShowUserOverview(true);
        setShowCurrentGames(false);
        setShowQueue(false);
    };

    const handleCurrentGameClick = () => {
        setShowUserOverview(false);
        setShowCurrentGames(true);
        setShowQueue(false);
    };

    const handleQueueClick = () => {
        setShowUserOverview(false);
        setShowCurrentGames(false);
        setShowQueue(true);
    };


    return (
        <>
            <Container>
                <Row className="mt-5">
                    <Col className="d-flex justify-content-center">
                        <h2 className="headline"><a onClick={handleCurrentGameClick}>LAUFENDE MATCHES</a></h2>
                    </Col>
                    <Col className="d-flex justify-content-center">
                        <h2 className="headline"><a onClick={handleUserOverviewClick}>NUTZERÜBERSICHT</a></h2>
                    </Col>
                    <Col className="d-flex justify-content-center mt-5">
                        <h2 className="headline"><a onClick={handleQueueClick}>AKTUELLE QUEUE</a></h2>
                    </Col>
                </Row>
                <Row>
                    {showUserOverview && <UserOverview/>}
                    {showCurrentGames && <RunningGames/>}
                    {showQueue && <MatchmakingQuwu/>}
                </Row>
            </Container>
        </>
    )
}