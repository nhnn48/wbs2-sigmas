import "./userOverview.css";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import GetAllGames from "./subComps/getAllGames.tsx";



export default function RunningGames() {


    return (
        <>
                <Container className="mt-5 bg-light rounded-2 p-5">
                    <Row>
                        <Col>
                            <GetAllGames/>
                        </Col>
                    </Row>
                </Container>
        </>
    )
}