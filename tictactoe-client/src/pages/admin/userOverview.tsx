import "./userOverview.css";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import GetAll from "./subComps/getAll.tsx";


export default function UserOverview() {


    return (
        <>
                <Container className="mt-5 bg-light rounded-2">
                    <Row>
                        <Col>
                            <Container className="mt-3 mb-3 bg-dark-subtle rounded-2 p-2">
                                <Container className="mt-2 bg-light rounded-2 p-1">
                                    <Row>
                                        <Col sm={1}>
                                            <h3 className="text-dark">ID:</h3>
                                        </Col>
                                        <Col sm={3}>
                                            <h3 className="text-dark">Username:</h3>
                                        </Col>
                                        <Col sm={2}>
                                            <h3 className="text-dark">Vorname:</h3>
                                        </Col>
                                        <Col sm={2}>
                                            <h3 className="text-dark">Nachname:</h3>
                                        </Col>
                                        <Col sm={2}>
                                            <h3 className="text-dark">Elo:</h3>
                                        </Col>
                                        <Col sm={2}>
                                            <h3 className="text-dark">Profilbild:</h3>
                                        </Col>
                                    </Row>
                                    <GetAll/>
                                </Container>
                            </Container>
                        </Col>
                    </Row>
                </Container>
        </>
    )
}