import "./userOverview.css";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import PlayerInQueue from "./subComps/playerInQueue.tsx";



export default function MatchmakingQuwu() {


    return (
        <>
            <Container className="mt-5 bg-light rounded-2">
                <Row>
                    <Col>
                        <Container className="mt-3 mb-3 rounded-2">
                            <PlayerInQueue/>
                        </Container>
                    </Col>
                </Row>
            </Container>
        </>
    )
}