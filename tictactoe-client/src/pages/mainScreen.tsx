import {Col, Container, Image, Row} from "react-bootstrap";
import {useEffect, useState} from "react";
import logo from "../media/pictures/logo.png";
import cyanish from "../media/pictures/bg/cyanish.jpg";
import mainscreen from "../media/pictures/bg/mainscreen.jpg";
import "./mainScreen.css";

export default function MainScreen(
    props: { hideHeader: (boo: boolean) => void }
) {
    const [loggedIn, setLoggedIn] = useState<boolean>(false);

    /*
     * uses checkLogin to check if a user is currently logged in and set as the sessions currentUser
     * if thats the case the page will display the "whenLoggedIn" container
     * "whenLoggedOut" is displayed if the user is not logged in
     */
    useEffect(() => {

        const fetchLoginStatus = async () => {
            try {
                const res = await fetch("/session/checkLogin", {method: 'GET'});
                if (!res.ok) throw new Error("Well fck");
                const isLoggedIn = await res.json();
                if (!isLoggedIn) {
                    setLoggedIn(false);
                    props.hideHeader(true);
                    document.body.style.backgroundImage = `url(${cyanish})`;
                } else {
                    setLoggedIn(true);
                    props.hideHeader(false);
                    document.body.style.backgroundImage = `url(${mainscreen})`;
                    document.body.style.backgroundPositionY = "8vh";
                    document.body.style.backgroundAttachment = 'fixed';
                    document.body.style.overflow = "hidden";
                }
            } catch (err) {
                console.error(err);
            }
        };

        fetchLoginStatus();
    }, []);


    return (
        <>
            {loggedIn == false ?
                <Container className="whenLoggedOut d-flex justify-content-center align-items-center text-center">
                    <Row>
                        <Col sm={12}>
                            <h1 className="title">WILLKOMMEN BEI <br/>TICTACT </h1> <Image fluid src={logo} className="logoImg"/> <h1 className="title">ROYALE</h1>
                            <a href="/login"><button type="button" className="goButton mt-5">LETSA GO</button></a>
                        </Col>
                    </Row>

                </Container>
                :
                <Container className="whenLoggedIn">
                    <Row className="d-flex justify-content-center align-items-center text-center">
                        <Col sm={12}>
                            <h1 className="title"><a href="/quwu">SPIEL STARTEN</a></h1>
                        </Col>
                    </Row>
                </Container>
            }
        </>
    )
}