import { useState, useEffect } from 'react';
import "./stopwatch.css"

export default function Stopwatch() {
    const [time, setTime] = useState<number>(0);
    const [timer, setTimer] = useState<NodeJS.Timeout | null>(null);

    useEffect(() => {
        const newTimer = setInterval(() => {
            setTime(prevTime => prevTime + 1);
        }, 1000);
        setTimer(newTimer);

        return () => {
            if (timer) clearInterval(timer);
        };
    }, []);

    /*
     * formats the time to the 00:00h format
     */
    const formatTime = (): string => {
        const hours: number = Math.floor(time / 3600);
        const minutes: number = Math.floor((time % 3600) / 60);
        const seconds: number = time % 60;

        const pad = (num: number): string => {
            return num.toString().padStart(2, '0');
        };

        return `${pad(hours)}:${pad(minutes)}:${pad(seconds)}`;
    };

    return (
        <div>
            <h1 className="timer">{formatTime().substring(3, 8)}</h1>
        </div>
    );
}