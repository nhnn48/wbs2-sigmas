export class RandomText{
    public static getRandomAssText(textid: number): any {
        switch (textid){
            case 1: {
                return "Among Tic, Among Tac, Among Toe - Finde den Impostor im Spielfeld und sichere dir den Sieg im Weltraum-duell";
            }
            case 2:{
                return "Swiggity Swooty im coming for that booty";
            }
            case 3:{
                return "Tic Tac Toe - UwU, das Spiel, bei dem selbst die Kawaii  Xs und Os um die Niedlichkeit konkurrieren! OWO";
            }
            case 4:{
                return "NYAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
            }
            case 5:{
                return "Wenn das Leben ein X gibt, zieh ein Kreis drum";
            }
            case 6:{
                return "Das große Spiel um den kleinen Deal! Platziere Kreise in einer Reihe, um zu gewinnen";
            }
            case 7:{
                return "Wurden Urzeitkrebse in der Urzeit Gegenwartskrebse genannt?";
            }
            case 8:{
                return "Pilot und Co-Pilot essen an Bord niemals das Gleiche. So ist es unwahrscheinlicher, dass beide gleichzeitig eine Lebensmittelvergiftung bekommen. 🤢";
            }
            case 9:{
                return "Das schwedische Forschungsprojekt 'Meowsic' untersucht, was die Laute von Katzen bedeuten und ob Katzen abhängig von ihrer Herkunft auch Dialekte 'sprechen'.";
            }
            case 10:{
                return "Sein oder nicht sein? Immer diese Fragen :(";
            }
        }
    }
}