import {Col, Row, Container, Image} from "react-bootstrap";
import {useEffect, useState} from "react";
import Stopwatch from "./stopwatch";
import socket from '../socket.tsx';
import bg from "../media/pictures/bg/queue.jpg";
import "./quwu.css";
import {RandomText} from "./randomText";

export default function Queue(
    props: { hideHeader: (boo: boolean) => void }
) {
    const [loggedPlayer, setLoggedPlayer] = useState({
        username: '',
        profilePic: '',
        elo: 0,
        id: -1,

    });
    const [enemyPlayer, setEnemyPlayer] = useState({
        username: '',
        profilePic: '',
        elo: 0,
    })
    const [currentText, setCurrentText] = useState<string>('');

    let game: number = 0;
    let interval: number = 0;


    useEffect(() => {

        localStorage.removeItem('gameId');
        /*
         * the user thats currently logged in as the sessions currentUser is fetched and set as the
         * loggedPlayer state
         */
        const getLoggedPlayer = async () => {
            props.hideHeader(true);
            try {
                const res = await fetch("/user", {method: 'GET'});
                if (!res.ok) throw new Error('bruh');
                const data = await res.json();
                const playerId = data.id;
                socket.emit('addQueue', {playerId});
                setLoggedPlayer({username: data.username, elo: data.elo, profilePic: data.profilePic, id: data.id});
            } catch (err) {
                console.error(err);
            }
        };

        /*
         * adds the currentUser to the queue
         */
        const addToQueue = async () => {

            try {
                await fetch("/queue", {
                    method: 'POST',
                    headers: {
                        "Content-type": "application/json"
                    },
                });
            } catch (err) {
                console.error(err);
            }
        }

        /*
         * gets a random text from the RandomText class and sets it as the currentText state
         */
        const randomassText = () => {
            let rndmNum: number = Math.floor(Math.random() * 10)+1;
            let getText: string = RandomText.getRandomAssText(rndmNum);
            setCurrentText(getText);
        }

        /*
         * the function is called every second and checks the queue for any matching players
         * if one is found the interval is cleared and gets the data of the enemyPlayer state
         */
        const checkQueue = async () => {
            try {
                const res = await fetch("/queue/" + interval, {method: 'GET'});
                const id = await res.json();
                game = Number(id.id);
                if (game == 1) {
                    setEnemyPlayer({
                        username: id.enemyUsername,
                        profilePic: id.enemyPfp,
                        elo: id.enemyElo
                    });
                }
            } catch (err) {
                console.error(err);
            }

        }

        randomassText();
        setInterval(randomassText, 4000);
        getLoggedPlayer();
        addToQueue();
        let check = setInterval(() => {
            if (game == 0) {
                interval++;
                checkQueue();
            } else {
                setUpGame();
                clearInterval(check);
            }
        }, 1000);

        /*
         * removes the currentUser from the queue
         * triggers automatically when the site is left
         */
        const yoinkFromQ = async(event: BeforeUnloadEvent) =>{
            try {
                await fetch("/queue", {method: 'DELETE'});
            }catch (err){
            }

            event.preventDefault();
            event.returnValue = "";
        }

        window.addEventListener("beforeunload", yoinkFromQ);

    }, []);


    /*
     * clears both the loggedPlayer and enemyPlayer states and redirects to the gamePage
     */
    const setUpGame = () => {
        interval = 0;
        setTimeout(() => {
            setLoggedPlayer({
                username: '',
                profilePic: '',
                elo: 0,
                id: -1,
            })
            setEnemyPlayer({
                username: '',
                profilePic: '',
                elo: 0,
            })
            window.location.href = "/gamePage";
        }, 5000)
    }

    document.body.style.backgroundImage = `url(${bg})`;
    document.body.style.backgroundAttachment = 'fixed';
    document.body.style.backgroundRepeat = 'no-repeat';
    document.body.style.overflow = 'hidden';

    return (
        <>
            <Container fluid>
                <Row className="d-flex justify-content-center text-center text-center mt-5">
                    <Col>
                        <h1 className="quwuHeadline mb-3">GEGNER WIRD GESUCHT</h1>
                        <Stopwatch/>
                    </Col>
                </Row>
                <Row className="my-5"><Col sm={12}></Col></Row>
                <Row className="d-flex justify-content-between align-items-center text-center my-5">
                    <Col sm={2}>
                            <Image fluid
                                   src={`${window.location.protocol}//${window.location.host}/user/pfp/${loggedPlayer.profilePic}`}
                                   roundedCircle className="quwuPfp filled"/>
                    </Col>
                    <Col sm={2}>
                        <p className="quUserData">{loggedPlayer.username}</p>
                        <span className="quUserData">{loggedPlayer.elo}</span>
                    </Col>

                    <Col sm={4}>

                    </Col>

                    {enemyPlayer.username !== '' ?
                        <Col sm={2}>
                            <p className="quUserData">{enemyPlayer.username}</p>
                            <span className="quUserData">{enemyPlayer.elo}</span>
                        </Col>
                        :
                        <Col sm={2}>
                        </Col>
                    }
                    {enemyPlayer.username !== '' ?
                        <Col sm={2}>
                                <Image fluid
                                       src={`${window.location.protocol}//${window.location.host}/user/pfp/${enemyPlayer.profilePic}`}
                                       roundedCircle className="quwuPfp filled"/>
                        </Col>
                        :
                        <Col sm={2}>
                        </Col>

                    }
                </Row>
                <Row className="d-flex justify-content-center align-items-center text-center">
                    <Col sm={10} className="quwuSubText">

                        <p>{currentText}</p>

                    </Col>
                </Row>
            </Container>
        </>
    )
}