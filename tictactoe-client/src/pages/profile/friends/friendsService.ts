

// deletes either a friend request or a friendship
export const deleteFriend = async (id: number):Promise<void> => {
    try {
        const anotherRes = await fetch("/friends/friend", {
            method: 'DELETE',
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify({
                friendId: id
            }),
        })
        if (!anotherRes.ok) throw new Error("Es ist ein Fehler aufgetreten");
    } catch (err) {
        console.error('Es ist ein Fehler aufgetreten', err);
    }

}


// adds a friend or sends a friend request
export const addFriend = async (id: number):Promise<void> => {
    try {
        const anotherRes = await fetch("/friends/friend", {
            method: 'PUT',
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify({
                friendId: id
            }),
        })
        if (!anotherRes.ok) throw new Error("Es ist ein Fehler aufgetreten");
    } catch (err) {
        console.error('Es ist ein Fehler aufgetreten', err);
    }


}
