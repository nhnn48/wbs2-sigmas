import {Button, Modal } from "react-bootstrap";
import {useEffect, useState} from "react";
import socket from "../../../socket.tsx";

export default function InviteModal(
    props: { modalCalled: boolean, sessionUserId: number , challengerId: number}
)
{
    const [isActive, setActiveState] = useState<boolean>(false);


    useEffect(() => {
        if (props.modalCalled) {
            showModal();
        }
    }, [props.modalCalled]);


    const closeModal = () => {
        setActiveState(false);
    }


    const showModal = () => setActiveState(true);

    const acceptChallenge = (challenger: number) => {

        createGame()

        window.location.href = "/gamePage";
        socket.emit('challengeAccepted', {challengerId: challenger});
    }

    const denyChallenge = (challenger: number) => {

        socket.emit('challengeDenied', {challengerId: challenger});
    }

    const createGame = async () => {
        try {

            const body = {
                player1ID: props.sessionUserId,
                player2ID: props.challengerId
            };

            const response = await fetch('/game', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(body)
            });

            const data = await response.json();

            if (data.success) {
                console.log('Game was created successfully.');
            } else {
                console.error('Failed to create game.');
            }
        } catch (error) {
            console.error('Error:', error);
        }
    };


    return (
        <>
            <Modal show={isActive} onHide={closeModal} dialogClassName="modal-dark" style={{border: 'solid black 0.25rem'}} className="pwModal">
                <Modal.Header closeButton>
                    <Modal.Title>Jemand möchte gegen dich spielen!</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    Willst du die Herausforderung annehmen??
                </Modal.Body>
                <Modal.Footer style={{background: 'whitesmoke'}}>
                    <Button variant="success" onClick={() => { acceptChallenge(props.challengerId); closeModal(); }}>LETSGOO</Button>
                    <Button variant={"danger"} onClick={ () => {denyChallenge(props.challengerId); closeModal(); }}>lieber nicht :(</Button>
                </Modal.Footer>
            </Modal>
        </>
    )
}