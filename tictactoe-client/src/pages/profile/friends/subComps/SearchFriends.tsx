import {useState} from "react";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Image from "react-bootstrap/Image";
import {addFriend} from "../friendsService.ts";
import empty from "../../../../media/pictures/empty.png";
import "./friendlist.css";

interface User {
    id: number;
    profilePic: string;
    username: string;
    elo: number;
    rank?: number;
}


export default function SearchFriends() {
    const [searchQuery, setSearchQuery] = useState('');
    const [foundUsers, setFoundUsers] = useState<User[]>([]);

    const handleSearch = async (query: string) => {
        try {
            const response = await fetch(`/friends/search?username=${query}`);
            if (response.ok) {
                const data = await response.json();
                setFoundUsers(data);
            } else {
                throw new Error('Failed to fetch users');
            }
        } catch (error) {
            console.error('Error searching for users:', error);
            setFoundUsers([]);
        }
    };


    const handleChange = async (event: React.ChangeEvent<HTMLInputElement>) => {
        const { value } = event.target;
        setSearchQuery(value);
        if (value.length > 0) {
            await handleSearch(value);
        } else {
            setFoundUsers([]);
        }
    };

    return (
        <>
            <Row className="d-flex justify-content-center align-items-center">
            <input
                type="text"
                placeholder="Freund Suchen"
                value={searchQuery}
                onChange={handleChange}
                className="searchFriends"
            />
            </Row>
            <ul>
                {foundUsers.map((user) => (
                    <li key={user.id}>
                        <Row className="entryRow butRedRow d-flex justify-content-center align-items-center text-center">
                            <Col md={3}>
                                {user.profilePic === "empty.png"?
                                    <Image fluid src={empty} className="emptypfp"/>
                                    :
                                    <Image src={`${window.location.protocol}//${window.location.host}/user/pfp/${user.profilePic}`} roundedCircle fluid className="filled emptypfp"/>
                                }
                            </Col>
                            <Col md={4}>
                                <p className="searchedUsername">{user.username}</p>
                            </Col>
                            <Col md={3}>
                                <p className="searchedElo">{user.elo}</p>
                            </Col>
                            <Col md={1}>
                                <button className="goButtonSmol v-smol my-2" onClick={()=>addFriend(user.id)}>Freund hinzufügen</button>
                            </Col>
                            <Col md={1}></Col>
                        </Row>
                    </li>
                ))}
            </ul>
        </>
    );

}
