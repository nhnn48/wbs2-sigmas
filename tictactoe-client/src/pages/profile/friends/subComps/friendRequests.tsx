import {useEffect, useState} from "react";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Image from "react-bootstrap/Image";
import {addFriend, deleteFriend} from "../friendsService.ts";
import SearchFriends from "./SearchFriends.tsx";
import "./friendlist.css";
import {fetchSessionUser} from "../../../../service/sessionService.ts";
import socket from "../../../../socket.tsx";

interface User {
    id: number;
    profilePic: string;
    username: string;
    elo: number;
    rank?: number;
}


export default function FriendRequests(){
    const [users, setUsers] = useState<User[]>([]);
    const [currentUser, setUser] = useState<number>(0);


    useEffect(() => {
        fetchSessionUser().then((id)=> {
            if (id == undefined) {
                return
            }
            setUser(id);
        })

        getAndSetRequests()
    }, []);


    // gets a friend request if someone sends it and refreshes the page
    socket.on('friendlistUpdate', (id) => {
        if (currentUser == id) {
            getAndSetRequests();
        }
    });

    // loads the users friend requests
    const getAndSetRequests = () => {
        fetch('/friends/friendRequests')
            .then(response => response.json())
            .then((data: User[]) => {
                setUsers(data);
            })
            .catch(error => console.error('Error fetching users', error));
    }

    return(
        <>
            <Container className="leaderBoard butRed mt-5">
            <SearchFriends/>
            </Container>
            <Container className="leaderBoard butRed mt-5">
                <Row className="d-flex justify-content-center someTitle">Deine Freundschaftsanfragen</Row>
                {users.map((user: User) => (
                    <li key={user.id}>
                        <Row className="entryRow butRedRow d-flex justify-content-center align-items-center text-center">
                            <Col md={2}>
                                    <Image src={`${window.location.protocol}//${window.location.host}/user/pfp/${user.profilePic}`} roundedCircle fluid className="filled emptypfp"/>
                            </Col>
                            <Col md={3}>
                                <p className="searchedUsername">{user.username}</p>
                            </Col>
                            <Col md={2}>
                                <p className="searchedElo">{user.elo}</p>
                            </Col>
                            <Col md={2}>
                                <button className="goButtonSmol v-smol" onClick={()=> {addFriend(user.id).then(() => {
                                    getAndSetRequests()
                                })
                                }}
                                >Freund adden</button>
                            </Col>
                            <Col md={2}>
                                <button className="goButtonSmol  v-smol red me-2" onClick={()=> {
                                    //TODO
                                    deleteFriend(user.id).then(() => {
                                        getAndSetRequests()
                                    })
                                }} >Anfrage yoinken</button>
                            </Col>
                        </Row>
                    </li>
                ))}
            </Container>
        </>
    )
}
