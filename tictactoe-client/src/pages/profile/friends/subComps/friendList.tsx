import {useEffect, useState} from "react";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Image from "react-bootstrap/Image";
import {deleteFriend} from "../friendsService.ts";
import "./friendlist.css";
import socket from "../../../../socket.tsx";
import {fetchSessionUser} from "../../../../service/sessionService.ts";

interface User {
    id: number;
    profilePic: string;
    username: string;
    elo: number;
    rank?: number;
}

export default function FriendList(){

    const [users, setUsers] = useState<User[]>([]);
    const [currentUser, setUser] = useState<number>(0);

    useEffect(() => {
        fetchSessionUser().then((id)=> {
            if (id == undefined) {
                return
            }
            setUser(id);
        })
    }, []);


    // gets the users friends/ refreshes the friends list
    const getAndSetFriendsList = () => {
        fetch('/friends/friends')
            .then(response => response.json())
            .then((data: User[]) => {
                setUsers(data);
            })
            .catch(error => console.error('Error fetching users', error));
    }

    // sends a challenge request to a friend
    const challengeFriend = (userid: number) => {
        console.log(userid, currentUser);
        socket.emit('challengeFriend', { challengedId: userid, challengerId: currentUser});
    }

    // refreshes the friends list if someone completed the friendship
    socket.on('friendlistUpdate', (id) => {
        if (currentUser == id) {
            getAndSetFriendsList();
        }
    });

    useEffect(() => {
       getAndSetFriendsList()
    }, []);

    return(
        <>
            <Container className="leaderBoard butRed mt-5">
                {users.map((user: User) => (
                    <li key={user.id}>
                        <Row className="entryRow butRedRow d-flex justify-content-center align-items-center text-center">
                            <Col md={2}>
                                    <Image src={`${window.location.protocol}//${window.location.host}/user/pfp/${user.profilePic}`} roundedCircle fluid className="filled emptypfp"/>
                            </Col>
                            <Col md={4}>
                                <p className="searchedUsername">{user.username}</p>
                            </Col>
                            <Col md={1}>
                                <p className="searchedElo">{user.elo}</p>
                            </Col>
                            <Col md={2}>
                                <button className="goButtonSmol v-smol" type="button" onClick ={()=> {challengeFriend(user.id)}}>Challengen</button>
                            </Col>
                            <Col md={3}>
                                <button className="goButtonSmol v-smol red" onClick={()=> {
                                    deleteFriend(user.id).then(()=>{
                                        getAndSetFriendsList()
                                    })
                                }}>Freund yoinken :/</button>
                            </Col>
                        </Row>
                    </li>
                ))}
            </Container>
        </>
    )
}
