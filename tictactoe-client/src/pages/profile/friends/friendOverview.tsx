import {useState} from "react";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import FriendList from "./subComps/friendList.tsx";
import FriendRequests from "./subComps/friendRequests.tsx";
import bg from "../../../media/pictures/bg/red.jpg";
import "../ranks/ranksPage.css";
import "./subComps/friendlist.css";

export default function FriendOverview(){

    const [showFriends, setShowFriends] = useState(true);
    const [showRequests, setShowRequests] = useState(false);

    const handleFriendsClick = () => {
        setShowFriends(true);
        setShowRequests(false);
    };

    const handleRequestsClick = () => {
        setShowFriends(false);
        setShowRequests(true);
    };

    document.body.style.backgroundImage = `url(${bg})`;
    document.body.style.backgroundAttachment = 'fixed';
    document.body.style.backgroundRepeat = 'no-repeat';

    return(
        <>
            <Container fluid>
                <Row className="d-flex">
                    <Col sm={2} className="headline"></Col>
                </Row>
                <Row className="m-5">
                    <Col className="d-flex justify-content-start">
                        <h2 className="anchorHeadline"><a onClick={handleFriendsClick}>DEINE FREUNDE</a></h2>
                    </Col>
                    <Col className="d-flex justify-content-end">
                        <h2 className="anchorHeadline"><a onClick={handleRequestsClick}>FREUNDE ADDEN</a></h2>
                    </Col>
                </Row>
                <Row>
                    <Col sm={5} className="anchorCol"></Col>
                    <Col sm={2}></Col>
                    <Col sm={5} className="anchorCol"></Col>
                </Row>
                <Row>
                    {showFriends && <FriendList />}
                    {showRequests && <FriendRequests />}
                </Row>
            </Container>
        </>
    )
}
