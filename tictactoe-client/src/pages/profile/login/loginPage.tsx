import LoginForm from "./subComps/login";
import RegisterForm from "./subComps/register";
import "./loginPage.css";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import bg from "../../../media/pictures/bg/cyanish.jpg";

export default function LoginPage(){

    document.body.style.backgroundImage = `url(${bg})`;


    return(
        <>
            <Container className="mid">
                <Row>
                    <Col>
                        <LoginForm/>
                    </Col>
                    <Col>
                        <RegisterForm/>
                    </Col>
                </Row>
            </Container>
        </>
    )
}