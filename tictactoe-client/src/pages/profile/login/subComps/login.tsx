import "./login.css";
import {ChangeEvent, FormEvent, useState} from "react";
import Container from "react-bootstrap/Container";
import Form from "react-bootstrap/Form";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

export default function LoginForm() {
    const [userData, setUserData] = useState({
        username: '',
        password: ''
    });
    const [error, setError] = useState<string>('');

    /*
     * when a form input is changed the corresponding in the userData state will be updated
     */
    const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
        const {name, value} = e.target;
        setUserData(prevState => ({
            ...prevState,
            [name]: value
        }));
    }

    /*
     * when the form is submitted the userData will be used to log the user into the sessions currentUser thus
     * the user is logged in and redirecting the user to the mainpage
     * if the login data was wrong the user wont be logged in
     */
    const loginSubmit = async (e: FormEvent<HTMLFormElement>) => {
        e.preventDefault();

        try {
            const res = await fetch("/session/login", {
                method: 'POST',
                headers: {
                    "Content-Type": "application/json",
                },
                body: JSON.stringify({
                    username: userData.username,
                    password: userData.password
                })
            });
            if (!res.ok) {
                setError("Passwort oder Nutzername falsch oof");
                return;
            }
            setError('');
            setUserData({username: '', password: ''});
            window.location.href = "/";
        } catch (err) {
            console.error('Something went fishy ', err);
        }
    }

    return (
        <>
            <Container className="mt-5 formWinRed">
                <h2 className="text-center my-5 formTitle">ANMELDEN</h2>
                <Form onSubmit={loginSubmit}>
                    <Form.Group className="mx-5" controlId="loginUsername">
                        <Form.Label className="d-none">Nutzername</Form.Label>
                        <Form.Control type="text" onChange={handleChange} placeholder="Nutzername"
                                      name="username"></Form.Control>
                    </Form.Group>
                    <Form.Group className="mx-5" controlId="loginPassword">
                        <Form.Label className="d-none">Passwort</Form.Label>
                        <Form.Control type="password" onChange={handleChange} placeholder="Passwort nya"
                                      name="password"></Form.Control>
                    </Form.Group>
                    <Row className="d-flex justify-content-center mt-5">
                        {error && <p className="text-error">{error}</p>}

                        <Col sm={4}>
                            <button type="submit" className="goButtonSmol mt-5 me-5">
                                Anmelden
                            </button>
                        </Col>
                    </Row>
                </Form>
            </Container>
        </>
    )
}