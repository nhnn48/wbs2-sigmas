import "./register.css";
import {ChangeEvent, FormEvent, useState} from "react";
import Container from "react-bootstrap/Container";
import Form from "react-bootstrap/Form";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";

export default function RegisterForm(){
    const [userData, setUserData] = useState({
        firstName:'',
        lastName: '',
        username: '',
        password: '',
        passwordRe: ''
    });
    const [passwordError, setPasswordError] = useState('');

    /*
     * updates the userData after every change of the form inputs to always set the most recent state
     */
    const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
        const {name, value} = e.target;
        setUserData(prevState => ({
            ...prevState,
            [name]: value
        }));

        if (name === 'passwordRe'){
            if (userData.password !== value){
                setPasswordError('Passwörter sind nicht gleich');
            } else{
                setPasswordError('');
            }
        }
    }


    /*
     * when the form is submitted the userData will be used to create a new user with the corresponding values
     * when the user creation was succesful the user will automatically be logged in and set as the currentUser in the session
     */
    const registerSubmit = async (e: FormEvent<HTMLFormElement>): Promise<void> => {
        e.preventDefault();

        if (userData.password !== userData.passwordRe){
            setPasswordError('Passwörter sind nicht gleich uwu');
            return;
        }
        try {
            const res = await fetch("/user", {
                method: 'POST',
                headers: {
                    "Content-Type":"application/json",
                },
                body: JSON.stringify({
                    username: userData.username,
                    password: userData.password,
                    firstName: userData.firstName,
                    lastName: userData.lastName
                }),
            })
            if (!res.ok || res.status==400){
                const data = await res.json();
                setPasswordError(data.message);
                return;
            }
            else{
                try {
                    const anotherRes = await fetch("/session/login", {
                        method: 'POST',
                        headers: {
                            "Content-Type":"application/json",
                        },
                        body: JSON.stringify({
                            username: userData.username,
                            password: userData.password,
                        })
                    })
                    if (anotherRes.ok){
                        window.location.reload();
                        window.location.href = "/";
                    }
                } catch (err){
                    console.error("Something went fishy ",err);
                }
            }
        }catch (err){
            console.error("Something went fishy ",err);
        }
    }

    return(
        <>
            <Container className="my-5 formWinBlue">
                <h2 className="text-center my-5 formTitle">REGISTRIEREN</h2>
                <Form onSubmit={registerSubmit}>
                    <Form.Group className="mx-5" controlId="registerUsername">
                        <Form.Label className="d-none">Nutzername</Form.Label>
                        <Form.Control type="text" placeholder="Nutzername" name="username" onChange={handleChange}></Form.Control>
                    </Form.Group>
                    <Form.Group className="mx-5" controlId="registerEmail">
                        <Form.Label className="d-none">Dein Name</Form.Label>
                        <Row className="d-flex">
                            <Col>
                                <Form.Control type="text" placeholder="Vorname" name="firstName" onChange={handleChange}></Form.Control>
                            </Col>
                            <Col>
                                <Form.Control type="text" placeholder="Nachname" name="lastName" onChange={handleChange}></Form.Control>
                            </Col>
                        </Row>
                    </Form.Group>
                    <Form.Group className="mx-5" controlId="registerPassword">
                        <Form.Label className="d-none">Passwort</Form.Label>
                        <Row className="d-flex">
                            <Col>
                                <Form.Control type="password" placeholder="Passwort" name="password" onChange={handleChange}></Form.Control>
                            </Col>
                            <Col>
                                <Form.Control type="password" placeholder="Passwort WDH" name="passwordRe" onChange={handleChange}></Form.Control>
                            </Col>
                        </Row>
                    </Form.Group>
                    {passwordError && <p className="text-error">{passwordError}</p>}
                    <Row className="d-flex justify-content-center mt-3 me-5">
                        <Col sm={4}>
                            <button type="submit" className="goButtonSmol">
                                Registrieren
                            </button>
                        </Col>
                    </Row>
                </Form>
            </Container>
        </>
    )
}