
import "./rankingPage.css";
import Col from "react-bootstrap/Col";
import Image from "react-bootstrap/Image";
import heart from "../../../../media/pictures/heart.png";
import {useEffect, useState} from "react";
import {RankName} from "../../../../rankName";

export default function CurrentRank(){
    const [user, setUser] = useState({
        eloname: '',
        elo: 0,
    })

    /*
     * gets the current rank of the user that is logged in as the currentUser
     */
    useEffect(() => {
        const getUserData = async () =>{
            try {
                const res = await fetch("/user", {method: 'GET'});
                const data = await res.json();
                if (!res.ok) throw new Error('oh noes');
                const rankName: string = RankName.getRankName(Number(data.elo));
                setUser({
                    eloname: rankName,
                    elo: Number(data.elo),
                })
            } catch (err){
                console.error("eh ", err);
            }
        }

        getUserData();
    }, [])



    return(
        <>
            <Col className="text-center">
                <Image fluid src={heart} className="rankPicture"/>
                <p>{user.eloname}</p>
                <p>{user.elo}</p>
            </Col>
        </>
    )
}