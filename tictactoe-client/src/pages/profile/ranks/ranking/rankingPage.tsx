import "./rankingPage.css";
import RankShow from "./rankShow";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import CurrentRank from "./currentRank";
import Col from "react-bootstrap/Col";

export default function RankingPage(){

    return(
        <>
            <Container fluid>
                <Row className="d-flex justify-content-between mt-5 text-center">
                    <Col md={2} className="lowestRank">
                        <RankShow status={'lowest'}/>
                    </Col>
                    <Col md={2} className="lowerRank">
                        <RankShow status={'lower'}/>
                    </Col>
                    <Col md={3} className="currentRank">
                        <CurrentRank/>
                    </Col>
                    <Col md={2} className="higherRank">
                        <RankShow status={'higher'}/>
                    </Col>
                    <Col md={2} className="highestRank">
                        <RankShow status={'highest'}/>
                    </Col>
                </Row>
            </Container>
        </>
    )
}