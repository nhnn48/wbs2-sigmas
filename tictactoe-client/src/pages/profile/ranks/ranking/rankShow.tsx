
import "./rankingPage.css";
import Col from "react-bootstrap/Col";
import Image from "react-bootstrap/Image";
import heart from "../../../../media/pictures/heart.png";
import {useEffect, useState} from "react";
import {RankName} from "../../../../rankName";


export default function RankShow(
    props: {status: string}
){
    const [user, setUser] = useState({
        eloname: '',
        elo: 0,
    })

    /*
     * gets the rank of a user added or substracted by the given state
     * state: {1:lowest, 2: lower, 3: higher, 4: highest}
     */
    useEffect(()=>{
        const getRank = async () =>{
            try {
                const res = await fetch("/user/rank/"+props.status , {method: 'GET'})
                if (!res.ok) throw new Error("bro wth");
                const data = await res.json();
                if(data) {
                    let eloNum: number = RankName.getElo(data.name)
                    setUser({
                        eloname: data.name,
                        elo: eloNum
                    })
                }
            }catch (err){
            }
        }
        getRank();
    }, [])


    return(
        <>
            <Col className="text-center">
                <Image fluid src={heart} className="rankPicture"/>
                <p>{user.eloname}</p>
                <p>{user.elo}</p>
            </Col>
        </>
    )
}

