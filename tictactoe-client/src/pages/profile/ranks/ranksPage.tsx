import "./ranksPage.css";
import LeaderBoard from "./leaderboard/leaderBoard";
import RankingPage from "./ranking/rankingPage";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import {useState} from "react";
import bg from "../../../media/pictures/bg/orange.jpg";

export default function RankOverview() {

    const [showLeaderboard, setShowLeaderboard] = useState(false);
    const [showRankingPage, setShowRankingPage] = useState(true);

    /*
     * toggles visibilty of certain areas
     */
    const handleLeaderboardClick = () => {
        setShowLeaderboard(true);
        setShowRankingPage(false);
    };

    const handleRankingPageClick = () => {
        setShowLeaderboard(false);
        setShowRankingPage(true);
    };

    document.body.style.backgroundImage = `url(${bg})`;
    document.body.style.backgroundRepeat = 'no-repeat';
    document.body.style.backgroundAttachment = 'fixed';

    return (
        <>
            <Container fluid>
                <Row className="d-flex">
                    <Col sm={2} className="headline"></Col>
                </Row>
                    <Row className="m-5">
                        <Col className="d-flex justify-content-start">
                            <h2 className="anchorHeadline"><a onClick={handleRankingPageClick}>DEIN RANG</a></h2>
                        </Col>
                        <Col className="d-flex justify-content-end">
                            <h2 className="anchorHeadline"><a onClick={handleLeaderboardClick}>LEADERBOARD</a></h2>
                        </Col>
                    </Row>
                <Row>
                    <Col sm={5} className="anchorCol"></Col>
                    <Col sm={2}></Col>
                    <Col sm={5} className="anchorCol"></Col>
                </Row>
                <Row className="m-5">
                    {showLeaderboard && <LeaderBoard/>}
                    {showRankingPage && <RankingPage/>}
                </Row>
            </Container>
        </>
    )
}
