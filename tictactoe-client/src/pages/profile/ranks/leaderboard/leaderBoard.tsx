import "./leaderBoard.css";
import Container from "react-bootstrap/Container";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import {useEffect, useState} from "react";
import Image from "react-bootstrap/Image";
import empty from "../../../../media/pictures/empty.png";


interface User {
    id: number;
    profilePic: string;
    username: string;
    elo: number;
    rank?: number;
}


export default function LeaderBoard(){


    const [users, setUsers] = useState<User[]>([]);
    const [similarUsers, setSimilarUsers] = useState<User[]>([]);
    const [rank, setRank] = useState<number>(0);

    useEffect(() => {

        // gets the top 10 users with the highest elo
        fetch('/user/leaderboard/top10')
            .then(response => response.json())
            .then((data: User[]) => {
                setUsers(data);
            })
            .catch(error => console.error('Error fetching users', error));

        // gets the two users that are slightly better and worse than the current user
        fetch('/user/leaderboard/player')
            .then(response => response.json())
            .then((data: User[]) => {
                data.forEach((user, index) => {
                    user.rank = rank + index + 1;
                });
                setSimilarUsers(data);
            })
            .catch(error => console.error('Error fetching users', error));

        // gets the users position in the leaderboard
        fetch('/user/leaderboard/rank')
            .then(response => response.json())
            .then((data: number) => {
                setRank(data);
            })
            .catch(error => console.error('Error fetching users', error));
    }, [rank]);


    return(
        <>
            <Container className="leaderBoard mt-5">
                {users.map((user: User, index: number) => (
                    <li key={user.id}>
                <Row className="entryRow d-flex justify-content-center align-items-center">
                    <Col md={2}>
                        <h4>{index + 1}</h4>
                    </Col>
                    <Col md={2}>
                        { user.profilePic !== "empty.png" ?
                            <Image src={`${window.location.protocol}//${window.location.host}/user/pfp/${user.profilePic}`} fluid roundedCircle className="userPfp filled"/>
                            :
                            <Image fluid src={empty} className="userPfp" />
                        }
                    </Col>
                    <Col md={2}>
                        <h4>{user.username}</h4>
                    </Col>
                    <Col md={2}></Col>
                    <Col md={2}>
                        <h4>{user.elo}</h4>
                    </Col>
                </Row>
                    </li>
                ))}
                <Row className="divider d-flex justify-content-center">Deine Position</Row>
                {similarUsers.map((user: User) => (
                    <li key={user.id}>
                        <Row className="entryRow d-flex justify-content-center align-items-center">
                            <Col md={2}>
                                <h4>{user.rank}</h4>
                            </Col>
                            <Col md={2}>
                                { user.profilePic !== "empty.png" ?
                                    <Image src={`${window.location.protocol}//${window.location.host}/user/pfp/${user.profilePic}`} fluid roundedCircle className="userPfp filled"/>
                                    :
                                    <Image fluid src={empty} className="userPfp" />
                                }
                            </Col>
                            <Col md={2}>
                                <h4>{user.username}</h4>
                            </Col>
                            <Col md={2}></Col>
                            <Col md={2}>
                                <h4>{user.elo}</h4>
                            </Col>
                        </Row>
                    </li>
                ))}
            </Container>
        </>
    )
}