import "./profile.css";
import {PencilFill, TrashFill} from 'react-bootstrap-icons';
import "./subComps/userData.css";
import EditModal from "./subComps/pwModal";
import {ChangeEvent, useEffect, useState} from "react";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Image from "react-bootstrap/Image";
import bg from "../../../media/pictures/bg/orange.jpg";

export default function Profile(props: { onDeleteUser: () => void }) {

    const [user, setUser] = useState({
        firstname: '',
        lastname: '',
        username: '',
        profilePic: '',
        elo: ''
    });
    const [openModal, setOpenModal] = useState<boolean>(false);
    const [file, setFile] = useState<File | null>(null);
    const [pfpWin, setPfpWin] = useState<boolean>(false);

    useEffect(() => {
        getAndSetUser()
    }, []);

    /*
     * retrieves the data of the user that is currenty logged in and sets the user State to the fetched data
     */
    const getAndSetUser = () => {
        fetch('/user', {method: 'GET'})
            .then(response => {
                if (!response.ok) {
                    throw new Error('Failed to fetch users');
                }
                return response.json();
            })
            .then(data => {
                setUser(data);
            })
            .catch(error => {
                console.error('Error fetching users:', error);
            });
    }

    /*
     * toggles the visibility of the password edit modal
     */
    const handleModal = (): void => {
        if (!openModal) setOpenModal(true);
        else setOpenModal(false);
    }

    /*
     * if confirmed the user will be deleted and the user state cleared
     */
    const deleteAccount = async (): Promise<void> => {
        if (confirm("Wirklich löschen?")) {
            try {
                const res = await fetch("/user", {method: 'DELETE'});
                if (!res.ok) throw new Error('Something went fishy');
                props.onDeleteUser();
                setUser({firstname: '', lastname: '', username: '', profilePic: '', elo: ''});
                window.location.href = "/";
            } catch (err) {
                console.error('Something went fishy ', err);
            }
        } else return;
    }

    /*
     * uploads a png, jpeg or gif as the current users profile picture
     * the user will be re-rendered to show the new profile picture and hided the profile picture upload window
     */
    const submitPfpChange = async () => {
        if (!file) {
            try {
                const res = await fetch("/user/pfpreset", {
                    method: 'PUT',
                    headers: {
                        "Content-type":"application/json"
                    },
                });
                if (!res.ok) throw new Error("eh wth");
                const data = await res.json();
                setUser(data);
            } catch (err){
            }
            return;
        }

        const allowedTypes = ['image/png', 'image/jpeg', 'image/gif'];
        if (!allowedTypes.includes(file.type)) {
            alert('Bitte lade dein Bild im PNG, JPG oder GIF Format hoch');
            return;
        }

        const formData = new FormData();
        formData.append('file', file);
        try {
            const res = await fetch("/user/upload-profile-picture", {
                method: 'POST',
                body: formData
            });
            if (!res.ok) throw new Error('Something went fishy while uploading again');

            setPfpWin(false);
            setFile(null);
            getAndSetUser()
        } catch (err) {
            console.error('Something went fishy while uploading ', err);
        }
    }

    /*
     * sets the current uploaded file as the file state
     */
    const handlePfpChange = (e: ChangeEvent<HTMLInputElement>): void => {
        const selectedFile = e.target.files?.[0];
        if (selectedFile) {
            setFile(selectedFile);
        }
    }

    /*
     * toggles the visibility of the profile picture upload form
     */
    const enablePfpUpload = (): void => {
        if (pfpWin) setPfpWin(false);
        else setPfpWin(true);
    }

    document.body.style.backgroundImage = `url(${bg})`;
    document.body.style.backgroundSize = '110%, 110%';

    return (
        <>
            <Container>
                <Row className="d-flex justify-content-center mx-5 profileContent">
                    <Col sm={8} className="me-4 actualContent">
                        <Container>
                            <Row>
                                <Col sm={5}>
                                    <div className="text-center mt-2">
                                        <PencilFill onClick={enablePfpUpload} className="randomAssIcon"/>
                                        <TrashFill onClick={submitPfpChange} className="randomAssIcon"/>

                                        <Image
                                            src={`${window.location.protocol}//${window.location.host}/user/pfp/${user.profilePic}`}
                                            roundedCircle className="filled"/>
                                    </div>
                                </Col>
                                <Col className="text-center userData" sm={5}>
                                    <h2>{user.firstname} {user.lastname}</h2>
                                    <h2>{user.username}</h2>
                                    <h2>{user.elo}</h2>
                                </Col>
                                <Col sm={2}></Col>
                            </Row>
                        </Container>
                        {
                            pfpWin ?
                                <Container className="mt-2">
                                    <Row>
                                        <Col className="text-center">
                                            <input type="file" onChange={handlePfpChange}/>
                                            <button type="button" className="goButtonSmol" onClick={submitPfpChange}>
                                                Profilbild speichern
                                            </button>
                                        </Col>
                                    </Row>
                                </Container>
                                :
                                <p></p>
                        }


                    </Col>
                </Row>
                <Row className="d-flex justify-content-center mt-5">
                    <Col sm={4} className="ms-5">
                        <button className="goButton" type="button" onClick={handleModal}>
                            Passwort ändern
                        </button>
                    </Col>
                    <Col sm={4} >
                        <button type="button" className="goButtonSmol red" onClick={deleteAccount}>
                            Account Löschen
                        </button>
                    </Col>
                </Row>
                <EditModal modalCalled={openModal}/>
            </Container>
        </>
    )
}