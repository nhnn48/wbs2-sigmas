import {Button, Form, Modal } from "react-bootstrap";
import "./pwModal.css";
import {ChangeEvent, useEffect, useState} from "react";

export default function EditModal(
    props: { modalCalled: boolean }
)
{
    const [isActive, setActiveState] = useState<boolean>(false);
    const [passwordError, setPasswordError] = useState<string>('');
    const [password, setPassword] = useState({
        password: '',
        newPassword: '',
        newPwRe: ''
    });

    useEffect(() => {
        if (props.modalCalled) {
            showModal();
        }
    }, [props.modalCalled]);

    /*
     * resets the state of the modal and hides it
     */
    const closeModal = () => {
        setActiveState(false);
        if (password) setPassword({password: '', newPassword: '', newPwRe: ''});
        if (passwordError) setPasswordError('');
    }
    const showModal = () => setActiveState(true);

    /*
     * if a value of the form inputs is changed the password state is updated as such
     */
    const changingValues = (e: ChangeEvent<HTMLInputElement>) => {
        const {name, value} = e.target;
        setPassword(prevState => ({
            ...prevState,
            [name]: value
        }));

        if (name === 'newPwRe') {
            if (password.newPassword !== value) {
                setPasswordError('Passwörter sind nicht gleich');
            } else {
                setPasswordError('');
            }
        }
    };

    /*
     * when the form is submitted it checks if the entered old-password is matching the current users password,
     *  stored in the database
     * if matching the new password will be sent and set as the users new password
     */
    const saveEntry = async ():Promise<void> => {
        if (password.newPassword.length < 7) {
            try {
                const res = await fetch("/user/password/" + password.password, {method: 'GET'});
                if (res.status == 401) {
                    setPasswordError("Altes Passwort ist falsch oof");
                    return;
                } else {
                    try {
                        const anotherRes = await fetch("/user/password", {
                            method: 'PUT',
                            headers: {
                                "Content-Type": "application/json",
                            },
                            body: JSON.stringify({
                                password: password.newPassword
                            }),
                        })
                        if (!anotherRes.ok) throw new Error("wtf happened?");
                        closeModal();
                    } catch (err) {
                        console.error('Something went fishy ', err);
                    }
                }
            } catch (err) {
                console.error('Something went fishy ', err);
            }

        } else {
            setPasswordError('Passwort muss mindestens 8 zeichen haben');
        }
    }

    return (
        <>
            <Modal show={isActive} onHide={closeModal} dialogClassName="modal-dark" style={{border: 'solid black 0.25rem'}} className="pwModal">
                <Modal.Header closeButton>
                    <Modal.Title>Edit Password</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form>
                        <Form.Group className="mb-3">
                            <Form.Label>Altes Passwort</Form.Label>
                            <Form.Control style={{background: 'orange', color: 'cyan'}} type="password"
                                          name="password"
                                          onChange={changingValues}
                                          placeholder="Altes Passwort" autoFocus/>
                        </Form.Group>
                        <Form.Group className="mb-3">
                            <Form.Label>Neues Passwort</Form.Label>
                            <Form.Control style={{background: 'orange', color: 'cyan'}} type="password"
                                          name="newPassword"
                                          onChange={changingValues}
                                          placeholder="Neues Passwort" autoFocus/>
                        </Form.Group>
                        <Form.Group className="mb-3">
                            <Form.Label>Wiederhole neues Passwort</Form.Label>
                            <Form.Control style={{background: 'orange', color: 'cyan'}} type="password"
                                          name="newPwRe"
                                          onChange={changingValues}
                                          placeholder="WDH Neues Passwort" autoFocus/>
                        </Form.Group>
                    </Form>
                </Modal.Body>
                {passwordError && <p className="text-error">{passwordError}</p>}

                <Modal.Footer style={{background: 'whitesmoke'}}>
                    <Button variant={"success"} onClick={saveEntry}>Save and GOO</Button>
                    <Button variant={"danger"} onClick={closeModal}>Yoink and bye</Button>
                </Modal.Footer>
            </Modal>
        </>
    )
}