import "./statistics.css";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import {useEffect, useState} from "react";
import PastMatch from "./subComps/PastMatch.tsx";
import bg from "../../../media/pictures/bg/green.jpg";

export default function Statistics(){
    const [stats, setStats] = useState({
        wins: 0,
        losses: 0,
        draws: 0,
    });

    useEffect(() => {

        // gets the number of wins, losses and draws of the current user
        fetch('/game/stats', {method: 'GET'})
            .then(response => {
                if (!response.ok) {
                    throw new Error('Failed to fetch stats');
                }
                return response.json();
            })
            .then( async data => {
                setStats(data);
            })
            .catch(error => {
                console.error('Error fetching users:', error);
            });
    }, []);

    document.body.style.backgroundImage = `url(${bg})`;
    document.body.style.backgroundRepeat = 'no-repeat';
    document.body.style.backgroundAttachment = 'fixed';


    return(
        <>
            <Container fluid>
                <Row className="d-flex justify-content-center mt-5">
                    <Col sm={5} className="usernameHeadline ms-4">
                        <h1 className="d-flex">DEINE<b className="invisB">_</b>SPIELE</h1>
                    </Col>
                </Row>

                <Row className="d-flex justify-content-center mx-5 text-center mt-2 align-items-center statRow">
                    <Col md={1}></Col>
                    <Col md={3}>
                        <h4>{stats.wins} Sieg(e)</h4>
                    </Col>
                    <Col md={3}>
                        <h4>{stats.losses} Niederlage(n)</h4>
                    </Col>
                    <Col md={3}>
                        <h4>{stats.draws} Unentschieden</h4>
                    </Col>
                    <Col md={1}></Col>
                </Row>

                <Row className="mt-5">
                </Row>

                <PastMatch/>
            </Container>

        </>
    )
}
