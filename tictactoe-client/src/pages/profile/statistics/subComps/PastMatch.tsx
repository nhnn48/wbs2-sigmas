import "./PastMatch.css";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import {useEffect, useState} from "react";
import {fetchSessionUser} from "../../../../service/sessionService.ts";
import Image from "react-bootstrap/Image";
import o from "../../../../media/pictures/o blue.png";
import x from "../../../../media/pictures/x red.png";

interface Game {
    id: number;
    player1: {
        id: number;
        username: string;
        profilePic: string;
        elo: number;
    };
    player2: {
        id: number;
        username: string;
        profilePic: string;
        elo: number;
    };
    winner: number;
    timestamp: string;
    message: string;
}


export default function PastMatch() {

    const [games, setGames] = useState<Game[]>([]);
    const [userId, setUserID] = useState<number>(0);

    useEffect(() => {

        // gets the played matches of the current user
        fetch('/game/player')
            .then(response => response.json())
            .then((data: Game[]) => {
                setGames(data);
            })
            .catch(error => console.error('Error fetching games:', error));
        fetchSessionUser().then((id) => {
            if (id == undefined) {
                console.log("Hä")
                return
            }
            setUserID(id)
        })
    }, []);

    // returns either win, loss or draw depending on how the game went
    const showScore = (winner: number, player1: number, player2: number): string => {
        const id: number = userId
        if (winner == 0 && player1 === id) {
            return "Sieg"
        } else if (winner === 1 && player1 === id) {
            return "Niederlage"
        } else if (winner === 0 && player2 === id) {
            return "Niederlage"
        } else if (winner == 1 && player2 === id) {
            return "Sieg"
        } else if (winner == 2) {
            return "Unentschieden"
        }
        return "Fehler"
    }

    return (
        <>
            <Container fluid>
                <ul>
                    {games.map((game: Game) => (
                        <li key={game.id}>
                            <Row className="pastMatchRow d-flex justify-content-center align-items-center m-5 text-center">
                                <Col sm={1}>
                                        <Image className="matchPfp filled" fluid
                                               src={`${window.location.protocol}//${window.location.host}/user/pfp/${game.player1.profilePic}`}
                                               roundedCircle/>
                                </Col>
                                <Col sm={2} className="player1Data">
                                    <span className="userNameStat mb-3">{game.player1.username}</span><br/>
                                    <span className="eloStat">{game.player1.elo}</span>
                                </Col>
                                <Col sm={1}>
                                    <Image fluid src={x} className="gameIcon"/>
                                </Col>
                                <Col sm={1}>
                                    <Image fluid src={o} className="gameIcon"/>
                                </Col>
                                <Col sm={2} className="player2Data">
                                    <span className="userNameStat2 mb-3">{game.player2.username}</span><br/>
                                    <span className="eloStat2">{game.player2.elo}</span>
                                </Col>
                                <Col sm={1}>
                                        <Image className="matchPfp filled" fluid
                                               src={`${window.location.protocol}//${window.location.host}/user/pfp/${game.player2.profilePic}`}
                                               roundedCircle/>
                                </Col>
                                <Col sm={1} className="verticalLine"></Col>
                                <Col sm={2} className="leftLine">
                                    <h2> {showScore(game.winner, game.player1.id, game.player2.id)}</h2>
                                    <p></p>
                                </Col>
                            </Row>
                        </li>
                    ))}
                </ul>
            </Container>
        </>
    );
}
