
import '../../App.tsx';
import './board.css';
import {useEffect, useState} from "react";
import Square from './Square';
import Row from 'react-bootstrap/Row';
import socket from '../../socket.tsx';
import {Button, Modal} from "react-bootstrap";
import './endScreenModal.css';

interface User {
    id: number;
    username: string;
    elo: number
}

interface Game {
    winner: null | number;
    squares: any[];
    id: number;
    isNext: number;
    player1: User;
    player2: User;
    finished: boolean
}


export default function Board() {
    const [game, setGame] = useState<Game | null>(null);
    const [sessionUserId, setSessionUserId] = useState<number | null>(null);
    const [gameOutcome, setGameOutcome] = useState<"win" | "lose" | "draw" | null>(null);
    const [eloChange, setEloChange] = useState<number[]>([0,0]);
    const [eloDif, setEloDif] = useState<number>(0);
    const [showModal, setShowModal] = useState(false);

    useEffect(() => {

        /* sockets on gameStateUpdated (if other player makes move)
         *
         */
        const fetchDataNow = async () => {

            if (game) {

                socket.on('eloChange', (eloChanges: number[]) => {
                    setEloChange(eloChanges);
                });

                socket.on('gameStateUpdated', (updatedGameFromServer) => {
                    let updatedGame;
                    if (updatedGameFromServer.winner !== null) {
                        updatedGame = {
                            ...updatedGameFromServer,
                            isNext: null,
                            winner: updatedGameFromServer.winner
                        }
                        handleModalOpen();
                        localStorage.removeItem('gameId');
                        socket.disconnect();
                    } else {
                        updatedGame = {
                            ...updatedGameFromServer
                        }
                    }
                    setGame(updatedGame);
                });
            }


            // Cleanup socket connection
            return () => {
                socket.off('gameStateUpdated');
            };
        };

        fetchDataNow();

        /**
         * to handle the postGameModal: eloChanges set from the socket and show modal depending on sessionUser
         */
        if (game && game.winner !== null) {
            if (game.winner === 0 && game.player1.id == sessionUserId) {
                setGameOutcome("win");
                setEloDif(eloChange[0]);
            } else if (game.winner === 1 && game.player2.id == sessionUserId) {
                setGameOutcome("win");
                setEloDif(eloChange[1]);
            } else if (game.winner === 2) {
                setGameOutcome("draw");
                setEloDif(0);
            } else {
                setGameOutcome("lose");
                if (game.player1.id == sessionUserId) {
                    setEloDif(eloChange[0]);
                } else {
                    setEloDif(eloChange[1]);
                }
            }
        }
    }, [game]);

    useEffect(() => {
        const storedGameId = localStorage.getItem('gameId');

        /** if a game is stored in the local storage of a client, it will be rendered after a reload of the page, so that the client can continue to play
         *  else, the game will be fetched from the server and created (on first load of the board)
         */
        const fetchGameFromStoredId = async () => {
            if (storedGameId) {
                try {
                    const response = await fetch(`/game/currentgame/${storedGameId}`, {method: 'GET'});
                    if (response.ok) {
                        const game = await response.json();
                        socket.emit('joinGameRoom', { game: game, playerId: sessionUserId });

                        setGame(game);
                        const sessionUser = await fetchSessionUser();
                        setSessionUserId(sessionUser);
                    } else {
                        fetchData();
                    }
                } catch (error) {
                    console.error('Error fetching game:', error);
                }
            } else {
                fetchData();
            }
        };

        fetchGameFromStoredId();
    }, []);

    useEffect(() => {
        /**
         * send heartbeats (timestamps) to the socket server, to call still there
         */
        if (game && game.isNext !== sessionUserId) {
            sendHeartbeat();
            const heartbeatInterval = setInterval(sendHeartbeat, 15000); // Send heartbeat every 15 seconds

            // Cleanup function
            return () => {
                clearInterval(heartbeatInterval);
            };
        }
    }, [game?.isNext]);

    const sendHeartbeat = () => {
        socket.emit("heartbeat");
    };

    const handleModalOpen = (): void => {
        setShowModal(true);
    }

    const handleModalCloseBack = () => {
        setShowModal(false);
        localStorage.removeItem('gameId');
        window.location.href = "/";
    }
    const handleModalCloseQuwu = () => {
        setShowModal(false);
        localStorage.removeItem('gameId');
        window.location.href = "/quwu";
    }

    /**
     * fetch Game Data on first load and set all important const like sessionUser, gameId, etc. and fill the board with empty squares
     */
    const fetchData = async () => {
        try {
            const sessionUser = await fetchSessionUser();
            const gameData = await fetchGame();
            localStorage.setItem('gameId', gameData.id);

            if (!gameData || !sessionUser) {
                console.error('Failed to fetch game data or session user');
                return;
            }

            const initialSquares = Array(9).fill(null);

            setSessionUserId(sessionUser);

            const updatedGame: Game = {
                winner: null,
                squares: initialSquares,
                isNext: gameData.player1.id,
                player1: gameData.player1,
                player2: gameData.player2,
                id: gameData.id,
                finished: false
            };
            setGame(updatedGame);

        } catch (error) {
            console.error('Error fetching data:', error);
        }
    };

    /**
     * fetch session User, to determine his game status and look
     */
    const fetchSessionUser = async () => {
        try {
            const response = await fetch('/session/getSessionUser', {method: 'GET'});
            if (!response.ok) {
                throw new Error('Failed to fetch session user ID');
            }
            return await response.json();
        } catch (error) {
            console.error('Error fetching session user ID:', error);
        }
    };

    /**
     * fetch game from server
     */
    const fetchGame = async () => {
        try {
            const response = await fetch(`/game/unfinished`);
            if (!response.ok) {
                throw new Error('Failed to fetch game');
            }
            return await response.json();
        } catch (error) {
            console.error('Error fetching game:', error);
        }
    };

    /**
     * handle gamePlay and update it for the currentUSer, then emit socket for the enemy player to receive
     * @param i number of game square clicked
     * return if player isn't next or square already has a symbol in it
     */
    const handleClick = (i: number) => {
        if (!game || game.winner !== null || game.isNext !== sessionUserId || game.squares[i] !== null) {
            return;
        }
        const squaresCopy = [...game.squares];
        const currentPlayerSymbol = sessionUserId === game.player1.id ? 'X' : 'O';
        if (squaresCopy[i] === null) {
            squaresCopy[i] = currentPlayerSymbol;
        }
        let newIsNext
        if (game.isNext === game.player1.id) {
            newIsNext = game.player2.id;
        } else {
            newIsNext = game.player1.id;
        }
        const updatedGame: Game = {
            ...(game || {}),
            squares: squaresCopy,
            isNext: newIsNext
        };
        socket.emit("move", {gameId: game.id, index: i});
        setGame(updatedGame);

    }

    if (!game) {
        return <div>Loading....</div>;
    }


    return (
        <>
            <h2 className="text-center fs-2 nextPlayer" >
                {game.winner === null
                    ? 'Nächster Spieler: ' + (game.isNext === game.player1.id ? game.player1.username : game.player2.username)
                    : game.winner === 2
                        ? 'Unentschieden'
                        : 'Gewinner: ' + (game.winner === 0 ? game.player1.username : game.player2.username)
                }
            </h2>
            <div className="board">
                <Row className="board-row">
                    {game.squares.slice(0, 3).map((value, index) => (
                        <Square key={index} value={value} onClick={() => handleClick(index)}/>
                    ))}
                </Row>
                <Row className="board-row">
                    {game.squares.slice(3, 6).map((value, index) => (
                        <Square key={index + 3} value={value} onClick={() => handleClick(index + 3)}/>
                    ))}
                </Row>
                <Row className="board-row">
                    {game.squares.slice(6, 9).map((value, index) => (
                        <Square key={index + 6} value={value} onClick={() => handleClick(index + 6)}/>
                    ))}
                </Row>
            </div>
            <Modal show={showModal} onHide={handleModalCloseBack} dialogClassName="modal-dark modal-lg" style={{border: 'solid black 0.25rem'}} className="finishModal">
                <Modal.Body className="finishModalBody" style={{fontFamily: "Impact, sans-serif"}}>
                    {gameOutcome === "win" && (
                        <>
                            <h2>DU HAST GEWONNEN!</h2>
                            <p>Herzlichen Glückwunsch :) </p>
                            <p> + {eloDif} Punkte</p>
                        </>
                    )}
                    {gameOutcome === "lose" && (
                        <>
                            <h2>DU HAST VERLOREN!</h2>
                            <p>Viel Glück fürs nächste Mal :(</p>
                            <p> {eloDif} Punkte</p>
                        </>
                    )}
                    {gameOutcome === "draw" && (
                        <>
                            <h2>UNENTSCHIEDEN!</h2>
                            <p>Das war nicht besonders spannend :/ </p>
                            <p> +/- 0 Punkte</p>
                        </>
                    )}
                </Modal.Body>
                <Modal.Footer>
                    <Button variant={"success"} onClick={handleModalCloseQuwu}>erneut Spielen</Button>
                    <Button variant={"danger"} onClick={handleModalCloseBack}>Beenden</Button>
                </Modal.Footer>
            </Modal>

        </>
    );
}
