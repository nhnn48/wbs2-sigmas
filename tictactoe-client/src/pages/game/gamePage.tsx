import Board from "./board.tsx";
import "./gamePage.css";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import {useEffect, useState} from "react";
import Image from "react-bootstrap/Image";
import Modal from "react-bootstrap/Modal";
import {Button, ModalBody} from "react-bootstrap";
import socket from "../../socket.tsx";
import purple from "../../media/pictures/bg/purple.jpg";
import x from "../../media/pictures/x red.png";
import o from "../../media/pictures/o blue.png";


interface User {
    id: number;
    username: string;
    profilePic: string;
    elo: number;
}
interface GameData {
    player1: User;
    player2: User;
}


export default function GamePage(
    props: { hideHeader: (boo: boolean) => void }
){

    const [,setSessionUserId] = useState<number | null>(null);
    const [myUser, setMyUser] = useState<User | null>(null);
    const [enemyUser, setEnemyUser] = useState<User | null>(null);
    const [showModal, setShowModal] = useState(false);
    const [roomJoined, setRoomJoined] = useState(false);
    const [gameData, setGameData] = useState<GameData | null>(null);


    useEffect(() => {
        props.hideHeader(true);

        /**
         * fetch Game data to display myUser and enemy correctly
         */
        const fetchData = async () => {
            try {
                const sessionUser = await fetchSessionUser();
                const gameData = await fetchGame();

                if (!gameData || !sessionUser) {
                    console.error('Failed to fetch game data or session user');
                    return;
                }
                /**
                 * join gameRoom to receive only the moves in the game playing
                 */
                if (gameData && !roomJoined) {
                    localStorage.setItem('gameId', gameData.id);
                    socket.emit('authenticate', sessionUser);
                    socket.emit('joinGameRoom', { game: gameData, playerId: sessionUser });
                    setRoomJoined(true);
                }

                if(gameData.player1.id === sessionUser) {
                    setMyUser(gameData.player1);
                    setEnemyUser(gameData.player2);
                } else {
                    setMyUser(gameData.player2);
                    setEnemyUser(gameData.player1);
                }
                document.body.style.backgroundImage = `url(${purple})`;
                setSessionUserId(sessionUser);
                setGameData(gameData);
            } catch (error) {
                console.error('Error fetching data:', error);
            }
        };

        fetchData();

    }, []);


    /**
     * fetch Game data for users and id
     */
    const fetchGame = async () => {
        try {
            const response = await fetch(`/game/unfinished`);
            if (!response.ok) {
                throw new Error('Failed to fetch game');
            }
            return await response.json();
        } catch (error) {
            console.error('Error fetching game:', error);
        }
    };

    /**
     * fetch sessionUser to see if player1 or 2
     */
    const fetchSessionUser = async () => {
        try {
            const response = await fetch('/session/getSessionUser', { method: 'GET' });
            if (!response.ok) {
                throw new Error('Failed to fetch session user ID');
            }
            return await response.json();
        } catch (error) {
            console.error('Error fetching session user ID:', error);
        }
    };

    const handleModalClose = () => setShowModal(false);

    /*
    leave the game and loose it
     */
    const handleLeaveGame = () => {
        setShowModal(false);
        socket.emit('leftGame');
        localStorage.removeItem('gameId');
        window.location.href = "/";
    };

    const onClick = () => {
        setShowModal(true);
    };

    return (
        <Container className="mid">
            <Row>
                <Col sm={2} className="d-flex flex-column justify-content-center align-items-start">
                    <Image fluid
                           src={`${window.location.protocol}//${window.location.host}/user/pfp/${myUser ? myUser.profilePic : "loading.gif"}`}
                           roundedCircle className="quwuPfp filled"/>
                </Col>
                <Col sm={2} className="d-flex flex-column justify-content-center align-items-start">
                    <p className="quUserData">{myUser ? myUser.username : 'Loading...'}</p>
                    <p className="quUserData">{myUser ? myUser.elo : 'Loading...'}</p>
                </Col>
                <Col className="d-flex flex-column justify-content-center align-items-start">
                    {myUser && gameData && myUser.id === gameData.player1.id ? (
                        <Image fluid src={x} className="gameIcon"/>
                    ) : (
                        <Image fluid src={o} className="gameIcon"/>
                    )}
                </Col>
                <Col>
                </Col>
                <Col className="d-flex flex-column justify-content-center align-items-end">
                    {enemyUser && gameData && enemyUser.id === gameData.player1.id ? (
                        <Image fluid src={x} className="gameIcon"/>
                    ) : (
                        <Image fluid src={o} className="gameIcon"/>
                    )}
                </Col>
                <Col sm={2} className="d-flex flex-column justify-content-center align-items-end">
                    <p className="quUserData">{enemyUser ? enemyUser.username : 'Loading...'}</p>
                    <p className="quUserData">{enemyUser ? enemyUser.elo : "Loading"}</p>
                </Col>
                <Col sm={2} className="d-flex flex-column justify-content-center align-items-end">
                    <Image fluid
                           src={`${window.location.protocol}//${window.location.host}/user/pfp/${enemyUser ? enemyUser.profilePic : "loading.gif"}`}
                           roundedCircle className="quwuPfp filled"/>
                </Col>
            </Row>
            <Row className="align-items-end">
                <Col>
                </Col>
                <Col sm={10}>
                    <Board/>
                </Col>
                <Col>
                </Col>
            </Row>
            <Row>
                <button onClick={onClick} className="goButtonSmol red" style={{ marginTop: '-10rem', paddingTop: "2rem" }}>Spiel verlassen</button>
            </Row>

                <Modal show={showModal} onHide={handleModalClose}>
                    <Modal.Header closeButton>
                        <Modal.Title>Möchtest du das Spiel wirklich verlassen?</Modal.Title>
                        <ModalBody>Du verlierst dadurch automatisch gegen deinen Gegner!</ModalBody>
                    </Modal.Header>
                    <Modal.Footer style={{background: 'whitesmoke'}}>
                        <Button variant="success" onClick={handleModalClose}>
                            Nein
                        </Button>
                        <Button variant="danger" onClick={handleLeaveGame}>
                            Ja
                        </Button>
                    </Modal.Footer>
                </Modal>
            </Container>
    )
}
