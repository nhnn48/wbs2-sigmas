import React from 'react';
import { Button } from 'react-bootstrap';

interface SquareProps {
    value: string | null;
    onClick: () => void;
}

const Square: React.FC<SquareProps> = ({ value, onClick }) => {
    return (
        <Button className="square" onClick={onClick}>
            {value}
        </Button>
    );
};

export default Square;