export const fetchSessionUser = async () => {
    try {
        const response = await fetch('/session/getSessionUser');
        if (!response.ok) {
            throw new Error('Failed to fetch session user ID');
        }
        const data = await response.json();
        return(data as number);
    } catch (error) {
        console.error('Error fetching session user ID:', error);
    }
};

export const fetchSessionRole = async () => {
    try {
        const response = await fetch('/session/getSessionRole');
        if (!response.ok) {
            throw new Error('Failed to fetch session role');
        }
        const data = await response.text();
        console.log(data);
        return data || null;
    } catch (error) {
        console.error('Error fetching session user role:', error);
    }
};
