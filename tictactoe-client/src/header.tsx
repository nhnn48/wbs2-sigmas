import "bootstrap/dist/css/bootstrap.min.css";
import {BoxArrowRight} from 'react-bootstrap-icons';
import "./header.css";
import {BrowserRouter as Router, Route, Routes} from "react-router-dom";
import Profile from "./pages/profile/profile/profile";
import LoginPage from "./pages/profile/login/loginPage";
import Statistics from "./pages/profile/statistics/statistics";
import RankOverview from "./pages/profile/ranks/ranksPage";
import MainScreen from "./pages/mainScreen";
import {useEffect, useState} from "react";
import Navbar from "react-bootstrap/Navbar";
import Container from "react-bootstrap/Container";
import Nav from "react-bootstrap/Nav";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Image from 'react-bootstrap/Image';
import NavDropdown from "react-bootstrap/NavDropdown";
import empty_png from "./media/pictures/empty.png";
import empty1_png from "./media/pictures/empty1.png";
import AdminPage from "./pages/admin/adminPage.tsx";
import Queue from "./pages/quwu";
import GamePage from "./pages/game/gamePage";
import stats from "./media/pictures/stats.png";
import logo from "./media/pictures/logo.png";
import heart from "./media/pictures/heart.png";
import cock from "./media/pictures/cock.png";
import FriendOverview from "./pages/profile/friends/friendOverview.tsx";
import socket from "./socket.tsx";
import InviteModal from "./pages/profile/friends/InviteModal.tsx";
import {Alert} from "react-bootstrap";

export default function Header() {
    const [user, setUser] = useState({
        profilePic: empty1_png
    });
    const [isLogged, setIsLogged] = useState<boolean>(false);
    const [hidden, setHidden] = useState<boolean>(false);
    const [isAdmin, setAdmin] = useState<boolean>(false);
    const [sessionUserId, setSessionUserId] = useState<number>(0);
    const [openModal, setOpenModal] = useState<boolean>(false);
    const [challengerId, setChallengerId] = useState<number>(0);
    const [showAlert, setShowAlert] = useState(false);

    useEffect(() => {
        socket.on('challengeFriend', (userId, challengerId) => {

            if (sessionUserId == userId) {
                handleInviteModal();
                setChallengerId(challengerId)
            }
        });

        socket.on('challengeAccepted', (challengerId) => {

            if (sessionUserId == challengerId) {
                window.location.href = "/gamePage";
            }
        });

        socket.on('challengeDenied', (challengerId) => {

            if (sessionUserId == challengerId) {
                setShowAlert(true);
            }
        });
    },);

    useEffect(() => {
        if (openModal) {
            const timer = setTimeout(() => {
                console.log("Timeout reached. Closing modal...");
                setOpenModal(false);
            }, 200);
            return () => clearTimeout(timer);
        }
    }, [openModal]);


    /*
     * checks if a user is currenty logged in as the currentUser
     * if no user is logged in the page will be redirected to the main page
     * if the user is logged in the role be checked
     */
    useEffect(() => {
        const setCurUser = async () => {
            const sessId = await fetchSessionUser();
            if (sessId == undefined) {
                return
            }
            setSessionUserId(sessId);
        }
        const fetchLoginStatus = async () => {
            if (window.location.href.includes("login")) {
                setHidden(true);
            }
            try {
                const res = await fetch("/session/checkLogin", {method: 'GET'});

                const roleRes = await fetch("/session/checkAdmin", {method: 'GET'});
                const role = await roleRes.json();
                if (role) {
                    setAdmin(role);
                }

                const isLoggedIn = await res.json();
                if (!isLoggedIn && ( window.location.href.includes("profile") || window.location.href.includes("quwu") || window.location.href.includes("gamePage") || window.location.href.includes("statistics") || window.location.href.includes("rankoverview" || window.location.href.includes("rankPage"))
                )) {
                    setUser({profilePic: empty1_png});
                    setIsLogged(false);
                    return;
                } else {
                    try {
                        const anotherRes = await fetch("/user", {method: 'GET'});
                        if (!anotherRes.ok) throw new Error('Something went fishy');
                        const data = await anotherRes.json();
                        setUser({profilePic: data.profilePic})
                        setIsLogged(true);
                    } catch (err) {
                        console.error('Something went fishy ', err);
                    }
                }
            } catch (err) {
                console.error('Something went fishy ', err);
            }
        };
        fetchLoginStatus();
        setCurUser();
    }, [])


    const fetchSessionUser = async () => {
        try {
            const response = await fetch('/session/getSessionUser', {method: 'GET'});
            if (!response.ok) {
                throw new Error('Failed to fetch session user ID');
            }
            return await response.json();
        } catch (error) {
            console.error('Error fetching session user ID:', error);
        }
    };


    /*
     * the current user will be logged out and the session will be cleared
     * if the logout was succesful the page will be redirected to the main page
     */
    const logout = async (): Promise<void> => {
        try {
            const res = await fetch("/session/logout", {method: 'POST'})
            if (!res.ok) throw new Error('Something went fishy');
            window.location.href = "/";
            setUser({profilePic: empty1_png});
            setIsLogged(false);
            localStorage.removeItem('gameId');
        } catch (err) {
            console.error('Something went fish ', err);
        }
    }

    /*
     * hides the header
     */
    const hideHeader = (boo: boolean) => {
        setHidden(boo);
    }

    /*
     * toggles the visibility of the invitation modal
     */
    const handleInviteModal = (): void => {
        if (!openModal) setOpenModal(true);
        else setOpenModal(false);
    }

    return (
        <Router>
            {!hidden ?
                <Navbar expand="lg" className="bg-body-tertiary pt-3" id="navbar">
                    <Container>
                        <Navbar.Toggle aria-controls="basic-navbar-nav"/>
                        <Navbar.Collapse id="basic-navbar-nav">
                            <Nav className="w-100">
                                <Row className="w-100 d-flex justify-content-start align-items-start">
                                    <Col sm="4" className="text-end mt-2">
                                        <Col></Col>
                                        {!isAdmin ?
                                            <Col sm="2"></Col>
                                            :
                                            <Col sm="auto">
                                                <h3><Nav.Link href="/admin">Admin Page</Nav.Link></h3>
                                            </Col>
                                        }

                                    </Col>
                                    <Col sm="7">
                                        <Row className="d-flex justify-content-center">
                                            <Col sm="3" className="me-2 mt-2">
                                                <Navbar.Brand href="/"><Image fluid src={logo}/></Navbar.Brand>
                                            </Col>
                                            <Col sm="2">
                                                <Nav.Link href="/rankPage"><Image fluid src={stats} className="stats"/></Nav.Link>
                                            </Col>
                                            <Col sm="2">
                                                <Nav.Link href="/statistic"><Image fluid src={cock} className="stats"/></Nav.Link>
                                            </Col>
                                            {isLogged ?
                                                <Col sm="2">
                                                    {user.profilePic !== "empty.png" ?
                                                        <NavDropdown
                                                            title={<Image className="headerpfp filled headerfilled"
                                                                          fluid
                                                                          src={`${window.location.protocol}//${window.location.host}/user/pfp/${user.profilePic.toString()}`}
                                                                          roundedCircle/>}
                                                            id="basic-nav-dropdown">

                                                            <NavDropdown.Item href="/profile">Profil</NavDropdown.Item>
                                                            <NavDropdown.Item onClick={logout} href="/login">
                                                                <BoxArrowRight/>
                                                            </NavDropdown.Item>
                                                        </NavDropdown>
                                                        :
                                                        <NavDropdown
                                                            title={<Image fluid src={empty_png} className="headerpfp"/>}
                                                            id="basic-nav-dropdown">

                                                            <NavDropdown.Item href="/profile">Profil</NavDropdown.Item>
                                                            <NavDropdown.Item onClick={logout}>
                                                                <BoxArrowRight/>
                                                            </NavDropdown.Item>
                                                        </NavDropdown>
                                                    }
                                                </Col>
                                                :
                                                <Col sm="2">
                                                    <NavDropdown
                                                        title={<Image fluid src={empty1_png} className="headerpfp"/>}
                                                        id="basic-nav-dropdown">

                                                        <NavDropdown.Item href="/login"> <BoxArrowRight/>
                                                        </NavDropdown.Item>
                                                    </NavDropdown>
                                                </Col>

                                            }
                                            <Col sm="2">
                                                <Nav.Link href="/friendsList"><Image fluid src={heart}
                                                                                     className="stats mt-2"/></Nav.Link>
                                            </Col>
                                        </Row>
                                    </Col>
                                </Row>

                            </Nav>
                        </Navbar.Collapse>
                    </Container>
                    {showAlert && (

                        <Alert variant="danger" onClose={() => setShowAlert(false)} dismissible style={{ position: 'fixed', top: 0, left: 0, zIndex: 1000 }}>
                            Deine Herausforderung wurde abgelehnt.
                        </Alert>
                    )}
                    <InviteModal modalCalled={openModal} sessionUserId={sessionUserId} challengerId={challengerId}/>
                </Navbar>
                :
                <Container></Container>
            }

            <Routes>
                <Route path="/profile" element={<Profile onDeleteUser={logout}/>}></Route>
                <Route path="/login" element={<LoginPage/>}></Route>
                <Route path="/statistic" element={<Statistics/>}></Route>
                <Route path="/rankoverview" element={<RankOverview/>}></Route>
                <Route path="/admin" element={<AdminPage/>}></Route>
                <Route path="/" element={<MainScreen hideHeader={hideHeader}/>}></Route>
                <Route path="/quwu" element={<Queue hideHeader={hideHeader}/>}></Route>
                <Route path="/gamePage" element={<GamePage hideHeader={hideHeader}/>}></Route>
                <Route path="/friendsList" element={<FriendOverview/>}></Route>
                <Route path="/rankPage" element={<RankOverview/>}></Route>
            </Routes>
        </Router>
    )
}
