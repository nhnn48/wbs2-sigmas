export enum RanksEnum{
    Toesucker = 0,
    L = 1,
    Whack = 2,
    WienerSchwiener = 3,
    baka = 4,
    CTier = 6,
    OrdinärerGamer = 7,
    KrasserGamer = 8,
    EpicGamer = 9,
    Toemaster = 10,
    ZehbraZähmer = 11,
    TicTacTitan = 12,
    ProfiTicker = 13,
    AlbusToeSenpai = 14,
    xxSuperTurboSkillPersonxxNyaaa = 15
}