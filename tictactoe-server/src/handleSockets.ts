import { Server, Socket } from 'socket.io';
import { Game } from './game/Game';
import { UserService } from './user.service/user.service'; // Import UserService
import { UserDB } from './database/UserDB';
import { GameService } from './game.service/game.service';
import {
  MessageBody,
  SubscribeMessage,
  WebSocketGateway,
  WebSocketServer,
  ConnectedSocket,
} from '@nestjs/websockets';
import { GameDB } from './database/GameDB';

const HEARTBEAT_INTERVAL = 15000; // 15 seconds
const DISCONNECT_TIMEOUT = 40000; // 40 seconds
const connectedSockets = {};

@WebSocketGateway()
export class HandleSockets {
  @WebSocketServer()
  server: Server;

  //GAME STUFF
  games: Map<number, Game> = new Map<number, Game>();
  clientHeartbeats: Map<string, number> = new Map();

  constructor(
    private readonly userService: UserService,
    private readonly gameService: GameService,
  ) {
    setInterval(this.checkHeartbeats.bind(this), HEARTBEAT_INTERVAL);
  }

  /**
   * check if clients in game send timestamps, used to determine if a client isn't moving or closes the window/leaves the gamePage
   */
  checkHeartbeats() {
    const now = Date.now();
    for (const [clientId, lastHeartbeat] of this.clientHeartbeats) {
      if (now - lastHeartbeat > DISCONNECT_TIMEOUT) {
        this.leaveGame(clientId);
      }
    }
  }
  // store user id in socket to help with leaveGame
  @SubscribeMessage('authenticate')
  handleAuthenticate(
    @MessageBody() userId: number,
    @ConnectedSocket() client: Socket,
  ) {
    client.data.userId = userId;
    connectedSockets[userId] = client;
  }
  //set TimeStamp at heartbeat of CLient
  @SubscribeMessage('heartbeat')
  handleHeartbeat(client: Socket) {
    this.clientHeartbeats.set(client.id, Date.now());
  }

  //Handle if a player leaves the Game or takes too long to move (for leaving page as well) (other player wins woheay)
  private async leaveGame(clientId: string) {
    let game: Game | undefined;
    for (const [_, currentGame] of this.games) {
      if (
        currentGame.player1Client === clientId ||
        currentGame.player2Client === clientId
      ) {
        game = currentGame;
        break;
      }
    }

    if (!game) {
      console.log('Player is not in any game');
      this.clientHeartbeats.delete(clientId);
      return;
    }
    if (game.player1Client === clientId) {
      game.winner = 1;
    } else if (game.player2Client === clientId) {
      game.winner = 0;
    }
    game.player1Client = null;
    game.player2Client = null;

    await this.updateGameToDB(game);

    this.server.to(`game-${game.id}`).emit('gameStateUpdated', game);
    this.clientHeartbeats.delete(game.player1Client);
    this.clientHeartbeats.delete(game.player2Client);
    this.games.delete(game.id);
    this.handleUpdateGames();
  }

  /**
   * update game to database after called (finish game) and set elo for players
   * @param game game to update
   * @private
   */
  private async updateGameToDB(game: Game) {
    const gameInDB = await this.gameService.findCurGame(game.player1.id);
    const gamePosted: GameDB = {
      ...gameInDB,
      winner: game.winner,
      finished: true,
    };
    const ePlayer1: number =
      1 / (1 + Math.pow(10, (game.player2.elo - game.player1.elo) / 400));
    const ePlayer2: number =
      1 / (1 + Math.pow(10, (game.player1.elo - game.player2.elo) / 400));
    let eloChanges: number[] = [];

    if (gamePosted.winner == 0) {
      gamePosted.player1.elo = Math.round(
        gamePosted.player1.elo + 20 * (1 - ePlayer1),
      );
      gamePosted.player2.elo = Math.round(
        gamePosted.player2.elo + 20 * (0 - ePlayer2),
      );
      eloChanges = [
        Math.round(20 * (1 - ePlayer1)),
        Math.round(20 * (0 - ePlayer2)),
      ];
    } else if (gamePosted.winner == 1) {
      gamePosted.player1.elo = Math.round(
        gamePosted.player1.elo + 20 * (0 - ePlayer1),
      );
      gamePosted.player2.elo = Math.round(
        gamePosted.player2.elo + 20 * (1 - ePlayer2),
      );
      eloChanges = [
        Math.round(20 * (0 - ePlayer1)),
        Math.round(20 * (1 - ePlayer2)),
      ];
    } else {
      gamePosted.player1.elo = Math.round(
        gamePosted.player1.elo + 20 * (0.5 - ePlayer1),
      );
      gamePosted.player2.elo = Math.round(
        gamePosted.player2.elo + 20 * (0.5 - ePlayer2),
      );
      eloChanges = [
        Math.round(20 * (0.5 - ePlayer1)),
        Math.round(20 * (0.5 - ePlayer2)),
      ];
    }
    await this.gameService.updateGame(gamePosted);
    await this.userService.updateElo(gamePosted.player1, gamePosted.player2);
    this.server.to(`game-${game.id}`).emit('eloChange', eloChanges);
  }

  /**
   * handle move logic after client emits move, save the gamestate and emit it to both players with updated isNext
   * if calculateWinner gets a winner, game will be postet
   * @param gameId
   * @param index where square is filled
   */
  @SubscribeMessage('move')
  async handleMove(
    @MessageBody() { gameId, index }: { gameId: number; index: number },
  ): Promise<void> {
    const game = this.games.get(gameId);
    if (!game) {
      console.log('No game to move in Dummy');
      return;
    }

    let currentPlayerSymbol: string;
    if (game.isNext === game.player1.id) {
      currentPlayerSymbol = 'X';
      game.isNext = game.player2.id;
    } else {
      currentPlayerSymbol = 'O';
      game.isNext = game.player1.id;
    }

    game.squares[index] = currentPlayerSymbol;
    game.winner = calculateWinner(game.squares);

    if (game.winner !== null) {
      game.player1Client = null;
      game.player2Client = null;
      await this.updateGameToDB(game);
      this.server.to(`game-${game.id}`).emit('gameStateUpdated', game);
      this.clientHeartbeats.delete(game.player1Client);
      this.clientHeartbeats.delete(game.player2Client);
      this.games.delete(game.id);
      this.handleUpdateGames();
    }
    this.server.to(`game-${game.id}`).emit('gameStateUpdated', game);
  }

  /**
   * let players join a gameRoom together to only receive updates valuable for them and in order to handle multiple games together
   * @param client
   * @param data
   */
  @SubscribeMessage('joinGameRoom')
  async handleJoinGameRoom(
    client: Socket,
    data: { game: Game; playerId: number },
  ): Promise<void> {
    const { game, playerId } = data;
    client.join(`game-${game.id}`);
    let socketGame = this.games.get(game.id);
    if (!socketGame && client.data.userId === game.player1.id) {
      socketGame = {
        winner: null,
        squares: Array(9).fill(null),
        id: game.id,
        player1Client: client.id,
        player2Client: null,
        player1: game.player1,
        player2: game.player2,
        isNext: game.player1.id,
      };
    } else if (!socketGame && client.data.userId === game.player2.id) {
      socketGame = {
        winner: null,
        squares: Array(9).fill(null),
        id: game.id,
        player1Client: null,
        player2Client: client.id,
        player1: game.player1,
        player2: game.player2,
        isNext: game.player1.id,
      };
    } else if (socketGame && client.data.userId === game.player1.id) {
      socketGame = {
        ...socketGame,
        player1Client: client.id,
      };
    } else {
      socketGame = {
        ...socketGame,
        player2Client: client.id,
      };
    }
    this.games.set(game.id, socketGame);

    const userToRemove: UserDB | undefined = this.queue.get(playerId);
    if (userToRemove) {
      this.queue.delete(playerId);
    }
  }

  //Handle if a player leaves the Game via Button or takes too long to move (for leaving page as well) (other player wins woheay)
  @SubscribeMessage('leftGame')
  handleLeaveGame(client: Socket): void {
    this.leaveGame(client.id);
  }

  /**
   * return state of current game, used in game.controller to set the game correctly after client reloads the game page
   * @param gameId
   */
  getCurGameById(gameId: number): Game | undefined {
    return this.games.get(gameId);
  }

  queue: Map<number, UserDB> = new Map<number, UserDB>();

  // Sends a queue array to the client
  @SubscribeMessage('getQueue')
  async handleGetQueue(client: Socket): Promise<void> {
    const queueArray = Array.from(this.queue.values());
    client.emit('queue', queueArray);
  }

  // Adds users to the queue based on the ID, creates an array from the queue and sends the current queue to the client
  @SubscribeMessage('addQueue')
  async getQueue(
    @MessageBody() { playerId }: { playerId: number },
  ): Promise<void> {
    const user: UserDB = await this.userService.getUserById(playerId);
    this.queue.set(playerId, user);
    const queueArray = Array.from(this.queue.entries());
    this.server.emit('getQueue', queueArray);
  }

  removeFromQueue(playerId: number, playerId2: number | null) {
    if (playerId2 !== null) {
      this.queue.delete(playerId2);
    }
    this.queue.delete(playerId);
    const queueArray = Array.from(this.queue.entries());
    this.server.emit('getQueue', queueArray);
  }

  handleUpdateFriend(userId: number) {
    this.server.emit('friendlistUpdate', userId);
  }

  @SubscribeMessage('challengeFriend')
  handleChallengeFriend(
    @MessageBody()
    {
      challengedId,
      challengerId,
    }: {
      challengedId: number;
      challengerId: number;
    },
  ) {
    if (connectedSockets[challengedId]) {
      connectedSockets[challengedId].emit(
        'challengeFriend',
        challengedId,
        challengerId,
      );
    }
  }

  @SubscribeMessage('challengeAccepted')
  handleAcceptChallenge(
    @MessageBody() { challengerId }: { challengerId: number },
  ) {
    if (connectedSockets[challengerId]) {
      connectedSockets[challengerId].emit('challengeAccepted', challengerId);
    }
  }

  @SubscribeMessage('challengeDenied')
  handleDenyChallenge(
    @MessageBody() { challengerId }: { challengerId: number },
    @ConnectedSocket() socket: Socket,
  ) {
    const sockets = this.server.sockets.sockets;
    if (connectedSockets[challengerId]) {
      connectedSockets[challengerId].emit('challengeDenied', challengerId);
    }
  }

  // Socket handler to update the running matches
  handleUpdateGames() {
    this.server.emit('gameUpdate');
  }
}

//calculate the winner based on squares filled with one symbol
function calculateWinner(board: string[]): number | null {
  const lines = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6],
  ];

  for (let i = 0; i < lines.length; i++) {
    const [a, b, c] = lines[i];
    if (board[a] && board[a] === board[b] && board[a] === board[c]) {
      if (board[a] === 'X') return 0;
      return 1;
    }
  }
  if (board.every((square) => square !== null)) return 2;
  return null;
}
