import { Entity, Column, PrimaryGeneratedColumn, ManyToOne } from 'typeorm';
import { WinnerEnum } from './WinnerEnum';
import { UserDB } from './UserDB';

@Entity()
export class GameDB {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => UserDB, { onDelete: 'SET NULL' })
  player1: UserDB;

  @ManyToOne(() => UserDB, { onDelete: 'SET NULL' })
  player2: UserDB;

  @Column({ default: null })
  winner: WinnerEnum;

  @Column({ default: false })
  finished: boolean;

  @Column({ default: new Date().toISOString() })
  timestamp: string;
}
