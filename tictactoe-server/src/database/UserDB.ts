import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  OneToMany,
  ManyToMany,
  JoinTable,
} from 'typeorm';
import { GameDB } from './GameDB';

@Entity()
export class UserDB {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ default: 'user' })
  role: string;

  @Column()
  username: string;

  @Column()
  password: string;

  @Column()
  firstName: string;

  @Column()
  lastName: string;

  @Column({ default: 'empty.png' })
  profilePic: string;

  @Column({ default: 1000 })
  elo: number;

  @OneToMany((type) => GameDB, (game) => game.player1)
  gamesAsPlayer1: Promise<GameDB[]>;

  @OneToMany((type) => GameDB, (game) => game.player2)
  gamesAsPlayer2: Promise<GameDB[]>;

  @ManyToMany((type) => UserDB, (user) => user)
  @JoinTable({ name: 'friendsList' })
  friends: Promise<UserDB[]>;
}
