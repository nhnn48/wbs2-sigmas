import {
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Res,
  Session,
  UseGuards,
} from '@nestjs/common';
import { SessionData } from 'express-session';
import { UserService } from '../user.service/user.service';
import { GameService } from '../game.service/game.service';
import { RanksEnum } from '../database/RanksEnum';
import { GameDB } from '../database/GameDB';
import { Response } from 'express';
import { HandleSockets } from '../handleSockets';
import { ApiBearerAuth, ApiResponse, ApiTags } from '@nestjs/swagger';
import { OkDTO } from '../serverDTO/okDTO';
import { IsLoggedInGuard } from '../is-logged-in/is-logged-in.guard';

@ApiTags('queue')
@Controller('queue')
export class QueueController {
  //if unfinished games dont get queued up with yourself
  public queue: number[];

  constructor(
    private readonly userService: UserService,
    private readonly gameService: GameService,
    private readonly handleSockets: HandleSockets,
  ) {
    this.queue = [];
  }

  /*
   * checks the elo to return the matching RankEnum
   * @Param elo is a number to determine the RankEnum
   * @Return the matching RankEnum
   */
  private getRank(elo: number): RanksEnum {
    if (elo < 150) return 0;
    else if (elo < 300) return 1;
    else if (elo < 450) return 2;
    else if (elo < 600) return 3;
    else if (elo < 750) return 4;
    else if (elo < 900) return 5;
    else if (elo < 1050) return 6;
    else if (elo < 1200) return 7;
    else if (elo < 1350) return 8;
    else if (elo < 1500) return 9;
    else if (elo < 1650) return 10;
    else if (elo < 1800) return 11;
    else if (elo < 1950) return 12;
    else if (elo < 2100) return 13;
    else if (elo > 2250) return 14;
  }

  /*
   * iterates through the queue array to match two players, if no other player is waiting in the queue it stops
   *  compares the ranks of users that are waiting to match players with similar ranks
   *  the comparison range extends after every two tries of checking the queue
   *  if two players are matched they are removed from the queue and the player that was added to the queue
   *  earlier than the other "creates the game"
   * @Pre the user is logged in as the current user
   * @Pre the user is already added to the queue
   * @Param tries which is the number of times the client asked for an update of the current queue
   */
  @ApiResponse({ type: OkDTO, description: 'matches two players in the queue' })
  @Get(':tries')
  @ApiBearerAuth()
  @UseGuards(IsLoggedInGuard)
  async checkQueue(
    @Res() res: Response,
    @Session() session: SessionData,
    @Param('tries') intervalInterval: number,
  ) {
    let game: GameDB;
    let enemy: number;
    let curUserRank: RanksEnum;

    try {
      if (this.queue.length > 0) {
        const userRN = await this.userService.getUserById(session.currentUser);
        curUserRank = this.getRank(userRN.elo);
        for (let i = 0; i < this.queue.length; i++) {
          if (this.queue[i] == userRN.id) continue;
          if (intervalInterval >= 14) {
            enemy = i;
            break;
          } else {
            for (let j = 0; j <= 14; j++) {
              if (
                intervalInterval > j * 2 &&
                (curUserRank -
                  this.getRank(
                    (await this.userService.getUserById(this.queue[i])).elo,
                  )) %
                  15 ==
                  j
              ) {
                enemy = i;
                break;
              } else if (intervalInterval < j * 2) break;
            }
          }
        }
        if (enemy != null) {
          const enemyPlayer = await this.userService.getUserById(
            this.queue[enemy],
          );

          let glockVergleich: number;
          for (let j = 0; j < this.queue.length; j++) {
            if (this.queue[j] === userRN.id) {
              glockVergleich = j;
              break;
            }
          }

          if (glockVergleich > enemy) {
            game = await this.gameService.createGame(userRN, enemyPlayer);
            this.handleSockets.handleUpdateGames();
            this.queue.splice(enemy, 1);
            for (let j = 0; j < this.queue.length; j++) {
              if (this.queue[j] === userRN.id) {
                glockVergleich = j;
                break;
              }
            }
            this.queue.splice(glockVergleich, 1);
            this.handleSockets.removeFromQueue(userRN.id, enemyPlayer.id);
          }
          res.json({
            id: 1,
            enemyUsername: enemyPlayer.username,
            enemyPfp: enemyPlayer.profilePic,
            enemyElo: enemyPlayer.elo,
          });
        } else {
          res.json({ id: 0 });
        }
      }
    } catch (err) {}
  }

  /*
   * adds the current user to the Playing-Queue if the user has not been added yet
   * @Pre the user is logged in as the current user
   */
  @ApiResponse({
    type: OkDTO,
    description: 'adds the current user to the queue',
  })
  @Post()
  @ApiBearerAuth()
  @UseGuards(IsLoggedInGuard)
  async addToQueue(@Session() session: SessionData): Promise<OkDTO> {
    const user = await this.userService.getUserById(session.currentUser);
    for (let i: number = 0; i < this.queue.length; i++) {
      const curUser = await this.userService.getUserById(this.queue[i]);
      if (curUser.id == user.id) {
        return;
      }
    }
    this.queue.push(session.currentUser);
    return new OkDTO(true, 'User was added to the queue');
  }

  /*
   * removes the current user from the Playing-queue
   * @Pre the user is logged in and added to the queue
   */
  @ApiResponse({
    type: OkDTO,
    description: 'removes the current user from the queue',
  })
  @Delete()
  @ApiBearerAuth()
  @UseGuards(IsLoggedInGuard)
  async removeFromQueue(@Session() session: SessionData): Promise<OkDTO> {
    const user = await this.userService.getUserById(session.currentUser);
    for (let i: number = 0; i < this.queue.length; i++) {
      const playerId = await this.userService.getUserById(this.queue[i]);
      if (playerId.id == user.id) {
        this.handleSockets.removeFromQueue(playerId.id, null);
        this.queue.splice(i, 1);
        return new OkDTO(true, 'User was removed the queue');
      }
    }
  }
}
