export class RankName {
  public static getRankName(elo: number): string {
    if (elo < 150) return 'Toesucker';
    else if (elo < 300) return 'L';
    else if (elo < 450) return 'Whack';
    else if (elo < 600) return 'WienerSchwiener';
    else if (elo < 750) return 'baka';
    else if (elo < 900) return 'CTier';
    else if (elo < 1050) return 'OrdinärerGamer';
    else if (elo < 1200) return 'KrasserGamer';
    else if (elo < 1350) return 'EpicGamer';
    else if (elo < 1500) return 'Toemaster';
    else if (elo < 1650) return 'ZehbraZähmer';
    else if (elo < 1800) return 'TicTacTitan';
    else if (elo < 1950) return 'ProfiTicker';
    else if (elo < 2100) return 'AlbusToeSenpai';
    else if (elo > 2250) return 'xxSuperSwaggerXX';
  }
}
