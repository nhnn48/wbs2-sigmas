import {
  BadRequestException,
  Body,
  Controller,
  Get,
  Post,
  Req,
  Res,
  Session,
  UnauthorizedException,
  UseGuards,
} from '@nestjs/common';
import { SessionData } from 'express-session';
import { Request, Response } from 'express';
import { UserDB } from '../database/UserDB';
import { DataSource, Repository } from 'typeorm';
import { LoginUserDTO } from '../user/DTO/LoginUserDTO';
import { ApiBearerAuth, ApiResponse, ApiTags } from '@nestjs/swagger';
import { LoginDTO } from '../user/DTO/LoginDTO';
import { OkDTO } from '../serverDTO/okDTO';
import { IsLoggedInGuard } from '../is-logged-in/is-logged-in.guard';

@ApiTags('session')
@Controller('session')
export class SessionController {
  private readonly userRepo: Repository<UserDB>;

  constructor(private userSource: DataSource) {
    this.userRepo = userSource.getRepository(UserDB);
  }

  /*
   *checks if the active is logged in
   *@Return boolean, true if the user is logged in, false if not
   */

  @ApiResponse({
    description: 'checks if the user is logged in and returns a boolean',
  })
  @Get('checkLogin')
  checkLogin(@Session() session: SessionData, @Res() res: Response): void {
    if (session.isLoggedIn === undefined) {
      res.json(false);
    } else {
      res.json(true);
    }
  }

  /*
   * checks if the user is an admin or an ordinary user
   * @Pre user is logged id
   * @Return boolean, true if the user is an admin, false if not
   */

  @ApiResponse({
    description: 'checks if the user is an admin and returns a boolean',
  })
  @Get('checkAdmin')
  @ApiBearerAuth()
  @UseGuards(IsLoggedInGuard)
  checkAdmin(@Session() session: SessionData, @Res() res: Response): void {
    if (session.role == 'admin') {
      res.json(true);
    } else {
      res.json(false);
    }
  }

  /*
   * sets the user, that logged in via the corresponding Login-Formular as the currentUser in the session
   * @Pre user has already a registered account
   * @Param username: the unique username that has been set when registering
   * @Param password: the password that matches with the password of the unique username in the Database
   * @Return LoginUserDTO which combines the important attributes when logging in an user
   * @Error if the password is empty
   * @Error if the password and username do not match
   */

  @ApiResponse({
    type: OkDTO,
    description:
      'logs in the user and pushes the important data in the session',
  })
  @Post('login')
  async login(
    @Session() session: SessionData,
    @Body() body: LoginDTO,
  ): Promise<OkDTO> {
    const loggedUser: LoginUserDTO = await this.userRepo.findOneBy({
      username: body.username,
      password: body.password,
    });
    if (body.password === '') {
      throw new UnauthorizedException();
    }
    if (loggedUser.role == 'admin') {
      session.currentUser = loggedUser.id;
      session.isLoggedIn = true;
      session.role = 'admin';
    } else if (loggedUser.role == 'user') {
      session.currentUser = loggedUser.id;
      session.role = 'user';
      session.isLoggedIn = true;
    } else {
      throw new UnauthorizedException();
    }
    return new OkDTO(true, 'user successfully logged in');
  }

  /*
   * removes the currentUser from the session, cleares the session
   * @Pre the user is currently set as the active user
   * @Error if the user is not logged in
   */

  @ApiResponse({
    type: OkDTO,
    description: 'logs out the user and deletes the session data',
  })
  @Post('logout')
  @ApiBearerAuth()
  @UseGuards(IsLoggedInGuard)
  logout(@Session() session: SessionData, @Req() req: Request): OkDTO {
    if (session.isLoggedIn == true) {
      session.isLoggedIn = undefined;
      session.currentUser = undefined;
      session.role = undefined;
      return new OkDTO(true, 'user successfully logged out');
    } else {
      throw new BadRequestException();
    }
  }

  /*
   * gets the id of the user that is currently active and set in the session
   * @Return undefined if no user is logged in
   * @Return the id of the currentUser
   */

  @ApiResponse({ description: 'fetches the currently logged in users ID' })
  @Get('getSessionUser')
  @ApiBearerAuth()
  @UseGuards(IsLoggedInGuard)
  getSessionUser(@Session() session: SessionData): number | undefined {
    return session.currentUser;
  }

  /*
   * gets the role of the currently logged in user
   * @Return the role as a JSON object
   */
  @ApiResponse({ description: 'fetches the currently logged in users role' })
  @Get('getSessionRole')
  @ApiBearerAuth()
  @UseGuards(IsLoggedInGuard)
  getSessionRole(@Session() session: SessionData, @Res() res: Response) {
    const role = session.role || '';
    res.send(role);
  }
}
