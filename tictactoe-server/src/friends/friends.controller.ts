import {
  Body,
  Controller,
  Delete,
  Get,
  Put,
  Query,
  Session,
  UseGuards,
} from '@nestjs/common';
import { ApiBearerAuth, ApiResponse, ApiTags } from '@nestjs/swagger';
import { GetOtherUserDTO } from '../user/DTO/GetOtherUserDTO';
import { UserService } from '../user.service/user.service';
import { HandleSockets } from '../handleSockets';
import { OkDTO } from '../serverDTO/okDTO';
import { IsLoggedInGuard } from '../is-logged-in/is-logged-in.guard';
import { SessionData } from 'express-session';
import { FriendDTO } from '../user/DTO/FriendDTO';
import { SearchUsersDto } from '../user/DTO/SearchUsersDTO';
import { UserDB } from '../database/UserDB';

@ApiTags('friends')
@Controller('friends')
export class FriendsController {
  constructor(
    private readonly userService: UserService,
    private readonly handleSockets: HandleSockets,
  ) {}

  //gets all users that are in the database
  @ApiResponse({
    type: [GetOtherUserDTO],
    description: 'gets all users for a regular user',
  })
  @Get('/potentialFriends')
  async getPotentialFriends(): Promise<GetOtherUserDTO[]> {
    const users = await this.userService.getAllUsers();
    return users.map((user) => {
      return this.transformUserDBtoGetOtherUserDTO(user);
    });
  }

  //adds a friend to the users friendlist, either sending a request or completing the friendship
  @ApiResponse({
    type: OkDTO,
    description: 'adds a friend to the users friendlist',
  })
  @Put('/friend')
  @ApiBearerAuth()
  @UseGuards(IsLoggedInGuard)
  async createFriend(
    @Session() session: SessionData,
    @Body() body: FriendDTO,
  ): Promise<OkDTO> {
    const user = session.currentUser;
    const friend = body.friendId;
    await this.userService.createFriend(user, friend);
    this.handleSockets.handleUpdateFriend(friend);
    return new OkDTO(true, 'Friend was added');
  }

  //gets all friend requests from a user / all friendships that arent completed
  @ApiResponse({
    type: [GetOtherUserDTO],
    description: 'gets all of the users friend requests',
  })
  @Get('/friendRequests')
  @ApiBearerAuth()
  @UseGuards(IsLoggedInGuard)
  async getFriendRequests(
    @Session() session: SessionData,
  ): Promise<GetOtherUserDTO[]> {
    const id = session.currentUser;
    const friendRequests = await this.userService.findFriendRequests(id);
    return friendRequests.map((user) => {
      return this.transformUserDBtoGetOtherUserDTO(user);
    });
  }

  //gets all mutual friends from a user
  @ApiResponse({
    type: [GetOtherUserDTO],
    description: 'gets all of the users mutual friends',
  })
  @Get('/friends')
  @ApiBearerAuth()
  @UseGuards(IsLoggedInGuard)
  async getFriends(
    @Session() session: SessionData,
  ): Promise<GetOtherUserDTO[]> {
    const id = session.currentUser;
    const friends = await this.userService.findFriends(id);
    return friends.map((user) => {
      return this.transformUserDBtoGetOtherUserDTO(user);
    });
  }

  //deletes a friend from the users friendlist
  @ApiResponse({
    type: OkDTO,
    description: 'deletes a friend from the users friendlist',
  })
  @Delete('/friend')
  @ApiBearerAuth()
  @UseGuards(IsLoggedInGuard)
  async deleteFriend(
    @Session() session: SessionData,
    @Body() body: FriendDTO,
  ): Promise<OkDTO> {
    const user = session.currentUser;
    const friend = body.friendId;
    await this.userService.deleteFriend(user, friend);
    this.handleSockets.handleUpdateFriend(friend);
    return new OkDTO(true, 'Friendship was deleted');
  }

  //searches for an user by their username, using the data that was provided by the query. the position in the name doesnt matter
  @ApiResponse({
    type: [GetOtherUserDTO],
    description: 'searches for an user by their username',
  })
  @Get('/search')
  async searchUsers(
    @Query() query: SearchUsersDto,
    @Session() session: SessionData,
  ) {
    const { username } = query;
    const user = session.currentUser;
    const foundUsers = await this.userService.searchByUsername(username, user);
    return foundUsers.map((user) => {
      return this.transformUserDBtoGetOtherUserDTO(user);
    });
  }

  transformUserDBtoGetOtherUserDTO(user: UserDB): GetOtherUserDTO {
    const dto = new GetOtherUserDTO();
    dto.id = user.id;
    dto.username = user.username;
    dto.elo = user.elo;
    dto.profilePic = user.profilePic;
    return dto;
  }
}
