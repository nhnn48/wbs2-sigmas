import {
  Body,
  Controller,
  Get,
  NotFoundException,
  Param,
  ParseIntPipe,
  Post,
  Session,
  UseGuards,
} from '@nestjs/common';
import { GameService } from '../game.service/game.service';
import { OkDTO } from '../serverDTO/okDTO';
import { CreateGameDTO } from './DTO/CreateGameDTO';
import { UserService } from '../user.service/user.service';
import { ApiBearerAuth, ApiResponse, ApiTags } from '@nestjs/swagger';
import { FindGameResDTO } from './DTO/FindGameResDTO';
import { GameDB } from '../database/GameDB';
import { SessionData } from 'express-session';
import { PlayerStatsResDTO } from './DTO/PlayerStatsResDTO';
import { WinnerEnum } from '../database/WinnerEnum';
import { IsLoggedInGuard } from '../is-logged-in/is-logged-in.guard';
import { UserDB } from '../database/UserDB';
import { GetOtherUserDTO } from '../user/DTO/GetOtherUserDTO';
import { HandleSockets } from '../handleSockets';

//Hallo Nico
@ApiTags('game')
@Controller('game')
export class GameController {
  constructor(
    private readonly gameService: GameService,
    private readonly userService: UserService,
    private readonly handleSockets: HandleSockets,
  ) {}

  // gets a game that is currently being played depending on the finished value and on its id
  @ApiResponse({
    type: FindGameResDTO,
    description: 'gets a game which is in play by its id',
  })
  @Get('/currentgame/:id')
  async getCurGameById(@Param('id') id: string) {
    const gameId = parseInt(id);
    const game = this.handleSockets.getCurGameById(gameId);
    if (game) {
      return game;
    } else {
      console.error(`Game with ID ${gameId} not found`);
      throw new NotFoundException(`Game with ID ${gameId} not found`);
    }
  }

  // posts a game into the database, but sets the finished value to false
  @ApiResponse({
    type: OkDTO,
    description: 'posts a not played game into the database',
  })
  @Post()
  @ApiBearerAuth()
  @UseGuards(IsLoggedInGuard)
  async createGame(@Body() body: CreateGameDTO) {
    const player1 = await this.userService.getUserById(body.player1ID);
    const player2 = await this.userService.getUserById(body.player2ID);
    await this.gameService.createGame(player1, player2);
    this.handleSockets.handleUpdateGames();

    return new OkDTO(true, 'Game was created');
  }

  // gets every game that exists in the database
  @ApiResponse({
    type: [FindGameResDTO],
    description: 'gets every game that has been played',
  })
  @Get('/all')
  async findAllGames() {
    const games = await this.gameService.findAllGames();
    return games.map((game) => {
      return this.transformGameDBtoFindGameResDTO(game);
    });
  }

  // gets a single game based on its id
  @ApiResponse({
    type: FindGameResDTO,
    description: 'gets a single game based on its ID',
  })
  @Get('/game/:gameID')
  async findOneGame(
    @Param('gameID', ParseIntPipe) gameID: number,
  ): Promise<FindGameResDTO> {
    const game = await this.gameService.findOneGame(gameID);
    return this.transformGameDBtoFindGameResDTO(game);
  }

  // gets all games that were played by a specific user
  @ApiResponse({
    type: [FindGameResDTO],
    description:
      'gets all played games from a specific player using the players ID',
  })
  @Get('/player/:userID')
  async findPlayersGames(@Param('userID', ParseIntPipe) userID: number) {
    const games = await this.gameService.findPlayersGames(userID);
    return games.map((game) => {
      return this.transformGameDBtoFindGameResDTO(game);
    });
  }

  // gets the game that is currently being played by a specific user
  @ApiResponse({
    type: FindGameResDTO,
    description:
      "gets the current game based on the user's ID and based on the finished value",
  })
  @Get('/unfinished')
  @ApiBearerAuth()
  @UseGuards(IsLoggedInGuard)
  async findCurGame(@Session() session: SessionData) {
    const game = await this.gameService.findCurGame(session.currentUser);
    if (!game) {
      return null;
    }
    return { ...this.transformGameDBtoFindGameResDTO(game), id: game.id };
  }

  // Fetches all unfinished games
  @ApiResponse({
    type: [FindGameResDTO],
    description: 'gets all current games',
  })
  @Get('/unfinishedAll')
  async findAllCurGame() {
    const games = await this.gameService.findAllCurGames();
    if (!games) {
      return null;
    }
    return games.map((game) => {
      return this.transformGameDBtoFindGameResDTO(game);
    });
  }

  // gets all played games from a specific user using the id in the session
  @ApiResponse({
    type: [FindGameResDTO],
    description:
      'gets all played games from a specific player using the ID in the session',
  })
  @ApiBearerAuth()
  @UseGuards(IsLoggedInGuard)
  @Get('/player')
  async findPlayersGamesID(@Session() session: SessionData) {
    const id: number = session.currentUser;
    const games = await this.gameService.findPlayersGames(id);
    return games.map((game) => {
      return this.transformGameDBtoFindGameResDTO(game);
    });
  }

  // fetches the wins, losses and draws from a player
  @ApiResponse({
    type: [PlayerStatsResDTO],
    description:
      'gets the number of wins, losses and draws from a specific player',
  })
  @ApiBearerAuth()
  @UseGuards(IsLoggedInGuard)
  @Get('/stats')
  async calcStatistics(@Session() session: SessionData) {
    const id: number = session.currentUser;
    const games: GameDB[] = await this.gameService.findPlayersGames(id);
    let wins: number = 0;
    let losses: number = 0;
    let draws: number = 0;

    // checks who was the winner and if the id matches the users id
    for (const game of games) {
      if (game.winner === WinnerEnum.player1 && game.player1.id === id) {
        wins++;
      } else if (game.winner === WinnerEnum.player2 && game.player2.id === id) {
        wins++;
      } else if (game.winner === WinnerEnum.player1 && game.player2.id === id) {
        losses++;
      } else if (game.winner === WinnerEnum.player2 && game.player1.id === id) {
        losses++;
      } else if (
        game.winner === WinnerEnum.draw &&
        (game.player1.id === id || game.player2.id === id)
      ) {
        draws++;
      }
    }
    return this.transformToPlayerStatsResDTO([wins, losses, draws]);
  }

  private transformGameDBtoFindGameResDTO(game: GameDB): FindGameResDTO {
    const dto = new FindGameResDTO();
    dto.id = game.id;
    dto.player1 = this.transformUserDBtoGetOtherUserDTO(game.player1);
    dto.player2 = this.transformUserDBtoGetOtherUserDTO(game.player2);
    dto.winner = game.winner;
    return dto;
  }

  private transformToPlayerStatsResDTO(stats: number[]): PlayerStatsResDTO {
    const dto = new PlayerStatsResDTO();
    dto.wins = stats[0];
    dto.losses = stats[1];
    dto.draws = stats[2];
    return dto;
  }

  private transformUserDBtoGetOtherUserDTO(user: UserDB): GetOtherUserDTO {
    const dto = new GetOtherUserDTO();
    dto.id = user.id;
    dto.username = user.username;
    dto.elo = user.elo;
    dto.profilePic = user.profilePic;
    return dto;
  }
}
