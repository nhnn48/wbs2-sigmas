import { IsNotEmpty, IsNumber } from 'class-validator';
import { ApiParam, ApiProperty } from '@nestjs/swagger';
export class CreateGameDTO {
  @ApiProperty()
  @IsNumber()
  @IsNotEmpty()
  player1ID: number;

  @ApiProperty()
  @IsNumber()
  @IsNotEmpty()
  player2ID: number;
}
