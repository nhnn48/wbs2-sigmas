import { ApiProperty } from '@nestjs/swagger';
export class EloResDTO {
  @ApiProperty()
  player1eloDif: number;

  @ApiProperty()
  player2eloDif: number;
}
