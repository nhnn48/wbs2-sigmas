import { IsNotEmpty, IsNumber } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class UpdateGameDTO {
  @ApiProperty({
    description: '0 = player 1 is winner, 1 = player 2 is winner, 2 = draw',
  })
  @IsNumber()
  @IsNotEmpty()
  winner: number;
}
