import { ApiProperty } from '@nestjs/swagger';
export class PlayerStatsResDTO {
  @ApiProperty()
  wins: number;

  @ApiProperty()
  losses: number;

  @ApiProperty()
  draws: number;
}
