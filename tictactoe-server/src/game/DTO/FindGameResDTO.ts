import { ApiProperty } from '@nestjs/swagger';
import { GetOtherUserDTO } from '../../user/DTO/GetOtherUserDTO';
export class FindGameResDTO {
  @ApiProperty()
  id: number;

  @ApiProperty()
  player1: GetOtherUserDTO;

  @ApiProperty()
  player2: GetOtherUserDTO;

  @ApiProperty({
    description: '0 = player 1 is winner, 1 = player 2 is winner, 2 = draw',
  })
  winner: number;

  @ApiProperty()
  timestamp: string;

  @ApiProperty()
  private message: string;

  constructor() {
    this.message = 'Played games were found';
  }
}
