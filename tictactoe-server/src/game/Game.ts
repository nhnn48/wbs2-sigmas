import { User } from './User';

export interface Game {
  winner: null | number;
  squares: any[];
  id: number;
  isNext: number;
  player1Client: string; //Client Socket
  player2Client: string; //Other Client Socket
  player1: User;
  player2: User;
}
