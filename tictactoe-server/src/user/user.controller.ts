import {
  BadRequestException,
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseIntPipe,
  Post,
  Put,
  Res,
  Session,
  UploadedFile,
  UseGuards,
  UseInterceptors,
  InternalServerErrorException,
} from '@nestjs/common';
import { Response } from 'express';
import { IsLoggedInGuard } from '../is-logged-in/is-logged-in.guard';
import { UserDB } from '../database/UserDB';
import { CreateUserDTO } from './DTO/CreateUserDTO';
import {
  ApiBearerAuth,
  ApiConsumes,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { SessionData } from 'express-session';
import { UserService } from '../user.service/user.service';
import { FileInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { extname, join } from 'path';
import { GetOtherUserDTO } from './DTO/GetOtherUserDTO';
import { OkDTO } from '../serverDTO/okDTO';
import { GetUserDTO } from './DTO/GetUserDTO';
import { EditUserDTO } from './DTO/EditUserDTO';
import { PasswordDTO } from './DTO/PasswordDTO';
import { HandleSockets } from '../handleSockets';

@ApiTags('user')
@Controller('user')
export class UserController {
  constructor(
    private readonly userService: UserService,
    private readonly handleSockets: HandleSockets,
  ) {}

  /*
   * uses the userService to create a new user with the given values that are needed in order to create a new user
   * @Param body CreateUserDTO, username, firstname, lastname, password
   * @Return OkDTO if the user was succesfully created
   * @Error if the input values do not meet the given requirements
   * @Error if the user could not be created
   */
  @ApiResponse({ type: OkDTO, description: 'creates a new user' })
  @Post()
  async createUser(@Body() body: CreateUserDTO) {
    if (body.password.trim() === '' || body.password.trim().length == 0) {
      throw new BadRequestException('Passwort darf nicht leer sein');
    }
    if (body.password.trim().length < 8) {
      throw new BadRequestException(
        'Passwort muss mindestens 8 Zeichen lang sein',
      );
    }
    if (body.username.trim().length > 15) {
      throw new BadRequestException(
        'Nutzername darf maximal 15 Zeichen lang sein',
      );
    }
    if (body.username.trim().length == 0 || body.username.trim() === '') {
      throw new BadRequestException('Nutzername darf nicht leer sein');
    }
    if (body.firstName.trim().length == 0 || body.firstName.trim() === '') {
      throw new BadRequestException('Vorname darf nicht leer sein');
    }
    if (body.lastName.trim().length == 0 || body.lastName.trim() === '') {
      throw new BadRequestException('Nachname darf nicht leer sein');
    }
    try {
      await this.userService.createUser(
        body.firstName,
        body.lastName,
        body.username,
        body.password,
      );
      return new OkDTO(true, 'User was created');
    } catch (err) {
      throw new BadRequestException('Nutzername gibt es schon');
    }
  }

  /*
   * uses the userService to retrieve every user in the database
   * @Return all users as a GetUserDTO
   */
  @ApiResponse({
    type: [GetUserDTO],
    description: 'gets all users for the admin',
  })
  @Get('/all')
  async getAll(): Promise<GetUserDTO[]> {
    const users = await this.userService.getAllUsers();
    return users.map((user) => {
      return this.transformUserDBtoGetUserDTO(user);
    });
  }

  /*
   * uses the userService to retrieve a specific user with an entered id
   * @Param id as an unique identifier to fetch the user
   * @Return the retrieved user as a GetOtherUserDTO
   */
  @ApiResponse({
    type: GetOtherUserDTO,
    description: 'gets other specific user by their id',
  })
  @Get('/one/:id')
  async findOtherUser(@Param('id', ParseIntPipe) userID: number) {
    let user;
    try {
      user = await this.userService.getUserById(userID);
    } catch (err) {}
    return this.transformUserDBtoGetOtherUserDTO(user);
  }

  /*
   * uses the userService to retrieve the data of the user thats currently logged in
   * @Pre the user is set as the currentUser of the session
   * @Return the user that is currently logged in as a GetUserDTO
   */
  @ApiResponse({
    type: GetUserDTO,
    description: 'gets a specific user by their id',
  })
  @ApiBearerAuth()
  @UseGuards(IsLoggedInGuard)
  @Get()
  async getUser(@Session() session: SessionData): Promise<GetUserDTO> {
    const id = session.currentUser;
    const user = await this.userService.getUserById(id);
    return this.transformUserDBtoGetUserDTO(user);
  }

  /*
   * uses the userService to retrieve 10 users with the highest elo among all users
   * @Pre 10 users are saved in the database
   * @Return a GetOtherUserDTO array of 10 users
   */
  @ApiResponse({
    type: [GetOtherUserDTO],
    description: 'gets the top 10 users based on elo',
  })
  @Get('/leaderboard/top10')
  async getUsersByEloDesc() {
    const users = await this.userService.getUsersByEloDesc();
    return users.map((user) => {
      return this.transformUserDBtoGetOtherUserDTO(user);
    });
  }

  /*
   * uses the userService to retrieve 2 other users with one user having closest lower elo and the other having
   *  closest higher elo than the user that is currently logged in
   * @Pre the user is currently logged in as the currentUser
   * @Return a GetOtherUserDTO array of 2 users
   */
  @ApiResponse({
    type: [GetOtherUserDTO],
    description:
      'gets two users with closest elo to the current user descending and ascending',
  })
  @ApiBearerAuth()
  @UseGuards(IsLoggedInGuard)
  @Get('/leaderboard/player')
  async getUsersBySimilarElo(@Session() session: SessionData) {
    const users = await this.userService.getUsersBySimilarElo(
      session.currentUser,
    );
    return users.map((user) => {
      return this.transformUserDBtoGetOtherUserDTO(user);
    });
  }

  /*
   * uses the userSevice to retrieve the rank of the logged in user substracted by two ranks (two ranks lower)
   * @Pre the user is currently logged in as the currentUser
   * @Return a number that is the rank of the user
   */
  @ApiResponse({
    type: [GetOtherUserDTO],
    description:
      'returns the rank of the current user in the leaderboard minus 2',
  })
  @ApiBearerAuth()
  @UseGuards(IsLoggedInGuard)
  @Get('/leaderboard/rank')
  async getUserRank(@Session() session: SessionData) {
    return await this.userService.getRank(session.currentUser);
  }

  /*
   * uses the userService to check if the input password matches the current password of the user
   * @Pre the user is currently logged in as the currentUser
   * @Param a string of the password the user typed in
   * @Return a boolean, true if the password matches, false if not
   */
  @ApiResponse({
    description: 'checks the password and returns either true or false',
  })
  @ApiBearerAuth()
  @UseGuards(IsLoggedInGuard)
  @Get('/password/:password')
  async comparePw(
    @Param('password') password: string,
    @Session() session: SessionData,
  ): Promise<boolean> {
    const id = session.currentUser;
    const user = await this.userService.getUserById(id);
    if (password === user.password) {
      return true;
    } else {
      return false;
    }
  }

  /*
   * uses the userService to get ranks "surrounding" the rank of the current user
   * @Pre the user is currently logged in as the currentUser
   * @Param state which can be {1: lowest, 2: lower, 3: higher, 4: highest}
   * @Return the name of the rank given the state as a JSON object
   */
  @ApiResponse({ description: 'gets the surrounding ranks of an user' })
  @ApiBearerAuth()
  @UseGuards(IsLoggedInGuard)
  @Get('/rank/:state')
  async getRanks(
    @Param('state') state: string,
    @Session() session: SessionData,
    @Res() res: Response,
  ): Promise<void> {
    try {
      const name = this.userService.getCertainRank(
        (await this.userService.getUserById(session.currentUser)).elo,
        state,
      );
      res.json({ name: name });
    } catch (err) {
      throw new BadRequestException('something went wrong');
    }
  }

  /*
   * uses the userService to update the profile picture of the current user
   * @Pre the user is currently logged in as the currentUser
   * @Param a file as the profile picture image
   * @Return an OkDTO if it was succesful
   */
  @ApiResponse({
    type: OkDTO,
    description: 'posts a profile picture for a specific user',
  })
  @ApiBearerAuth()
  @UseGuards(IsLoggedInGuard)
  @Post('upload-profile-picture')
  @ApiConsumes('multipart/form-data')
  @UseInterceptors(
    FileInterceptor('file', {
      storage: diskStorage({
        destination: './uploads/profilepictures',
        filename: (req: any, file, callback) => {
          const randomName = Array(32)
            .fill(null)
            .map(() => Math.round(Math.random() * 16).toString(16))
            .join('');
          callback(null, `${randomName}${extname(file.originalname)}`);
        },
      }),
    }),
  )
  async uploadProfilePicture(
    @UploadedFile() file: Express.Multer.File,
    @Session() session: SessionData,
  ) {
    const id = session.currentUser;
    const user = await this.userService.getUserById(id);
    user.profilePic = file.filename;
    await this.userService.updateUser(user);

    return new OkDTO(true, 'Profile Picture Upload successfull');
  }

  /*
   * retrieves a given profile picture
   * @Param image as the path to the image
   * @Return the file of the image as a JSON object
   */
  @ApiResponse({ description: 'Fetches the profile picture of an user' })
  @Get('pfp/:image')
  async getPfp(@Param('image') image: string, @Res() res: Response) {
    try {
      const imgPath = join(process.cwd(), 'uploads', 'profilepictures', image);
      res.sendFile(imgPath);
    } catch (err) {
      return new InternalServerErrorException('Something went wrong ', err);
    }
  }

  /*
   * uses the userService to update the values of the current user
   * @Pre the user is currently logged in as the currentUser
   * @Param body as EditUserDTO with the values that can be changed for an user
   * @Return an OkDTO
   */
  @ApiResponse({
    type: OkDTO,
    description: 'updates a specific user by their id',
  })
  @Put()
  @ApiBearerAuth()
  @UseGuards(IsLoggedInGuard)
  async updateUser(
    @Session() session: SessionData,
    @Body() body: EditUserDTO,
  ): Promise<OkDTO> {
    const id = session.currentUser;
    const user = await this.userService.getUserById(id);
    user.firstName = body.firstName;
    user.lastName = body.lastName;
    user.username = body.username;
    await this.userService.updateUser(user);
    return new OkDTO(true, 'User was updated');
  }

  /*
   * uses the userService to change to profile picture of the current user to the default "empty.png"
   * @Pre the user is currently logged in as the currentUser
   * @Return a GetUserDTO with the changed profile picture
   */
  @ApiResponse({
    type: OkDTO,
    description: 'sets the profile picture of an user to the default empty.pnh',
  })
  @Put('pfpreset')
  @ApiBearerAuth()
  @UseGuards(IsLoggedInGuard)
  async resetPfp(@Session() session: SessionData): Promise<OkDTO> {
    return new OkDTO(true, 'Profile Pic was deleted');
  }

  /*
   * uses the userService to change the password of the current user
   * @Pre the user is currently logged in as the currentUser
   * @Param password that has been sent by the user
   * @Return an OkDTO
   */
  @ApiResponse({ type: OkDTO, description: 'updates a users password' })
  @Put('password')
  @ApiBearerAuth()
  @UseGuards(IsLoggedInGuard)
  async updatePassword(
    @Session() session: SessionData,
    @Body() body: PasswordDTO,
  ): Promise<OkDTO> {
    const id = session.currentUser;
    const user = await this.userService.getUserById(id);
    user.password = body.password;
    await this.userService.updateUser(user);
    return new OkDTO(true, 'password was updated');
  }

  /*
   * uses the userService to "delete" the current user, by clearing the password making it impossible
   *  to log into the user
   * @Pre the user is currently logged in as the currentUser
   */
  @ApiResponse({
    type: OkDTO,
    description: 'deletes a specific user by their id',
  })
  @Delete()
  @ApiBearerAuth()
  @UseGuards(IsLoggedInGuard)
  async deleteUser(@Session() session: SessionData): Promise<OkDTO> {
    await this.userService.deleteUser(session.currentUser);
    return new OkDTO(true, 'user was deleted');
  }

  /*
   * transforms a regular user object UserDB to a GetUserDTO
   * @Pre the user is stored in the database
   * @Return the transformed DTO
   */
  private transformUserDBtoGetUserDTO(user: UserDB): GetUserDTO {
    const dto = new GetUserDTO();
    dto.id = user.id;
    dto.username = user.username;
    dto.firstname = user.firstName;
    dto.lastname = user.lastName;
    dto.elo = user.elo;
    dto.profilePic = user.profilePic;
    return dto;
  }

  /*
   * transforms a regular user object UserDB to a GetOtherUserDTO
   * @Pre the user is stored in the database
   * @Return the transformed DTO
   */
  transformUserDBtoGetOtherUserDTO(user: UserDB): GetOtherUserDTO {
    const dto = new GetOtherUserDTO();
    dto.id = user.id;
    dto.username = user.username;
    dto.elo = user.elo;
    dto.profilePic = user.profilePic;
    return dto;
  }
}
