import { IsNotEmpty, IsString } from 'class-validator';
import { ApiParam, ApiProperty } from '@nestjs/swagger';

export class PasswordDTO {
  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  password: string;
}
