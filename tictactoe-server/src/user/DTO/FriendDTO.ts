import { IsNotEmpty, IsString } from 'class-validator';
import { ApiParam, ApiProperty } from '@nestjs/swagger';

export class FriendDTO {
  @ApiProperty()
  @IsNotEmpty()
  friendId: number;
}
