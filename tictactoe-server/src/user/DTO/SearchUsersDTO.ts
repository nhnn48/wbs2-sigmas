import { IsNotEmpty } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class SearchUsersDto {
  @ApiProperty()
  @IsNotEmpty()
  username: string;
}
