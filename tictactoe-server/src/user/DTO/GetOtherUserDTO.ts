import { ApiProperty } from '@nestjs/swagger';
export class GetOtherUserDTO {
  @ApiProperty()
  id: number;

  @ApiProperty()
  username: string;

  @ApiProperty()
  profilePic: string;

  @ApiProperty()
  elo: number;
}
