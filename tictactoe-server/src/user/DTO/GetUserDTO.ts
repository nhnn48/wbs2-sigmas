import { ApiProperty } from '@nestjs/swagger';

export class GetUserDTO {
  @ApiProperty()
  id: number;

  @ApiProperty()
  username: string;

  @ApiProperty()
  firstname: string;

  @ApiProperty()
  lastname: string;

  @ApiProperty()
  profilePic: string;

  @ApiProperty()
  elo: number;
}
