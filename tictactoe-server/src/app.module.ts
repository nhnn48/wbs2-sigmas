import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';
import { UserController } from './user/user.controller';
import { UserService } from './user.service/user.service';
import { SessionController } from './session/session.controller';
import { GameController } from './game/game.controller';
import { GameService } from './game.service/game.service';
import { UserDB } from './database/UserDB';
import { GameDB } from './database/GameDB';
import { FakerService } from './faker.service/faker.service';
import { QueueController } from './queue/queue.controller';
import { HandleSockets } from './handleSockets';
import { AdminMiddleware } from './admin-middleware/admin.middleware';
import { FriendsController } from './friends/friends.controller';

@Module({
  imports: [
    ServeStaticModule.forRoot({
      rootPath: `${process.cwd()}/../tictactoe-client/dist`,
    }),
    ServeStaticModule.forRoot({
      serveRoot: '/uploads',
      rootPath: join(__dirname, '..', 'uploads'),
    }),
    TypeOrmModule.forRoot({
      type: 'sqlite',
      database: './db.sqlite',
      entities: [UserDB, GameDB],
      synchronize: true,
    }),
    TypeOrmModule.forFeature([UserDB, GameDB]),
  ],
  controllers: [
    UserController,
    SessionController,
    GameController,
    QueueController,
    FriendsController,
  ],
  providers: [UserService, GameService, FakerService, HandleSockets],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer): any {
    consumer.apply(AdminMiddleware).forRoutes('/admin');
  }
}
