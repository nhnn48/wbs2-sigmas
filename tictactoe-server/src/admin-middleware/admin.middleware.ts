import { Injectable, NestMiddleware } from '@nestjs/common';
import { Request, Response, NextFunction } from 'express';

// Checks if the user in the session is an admin. If not, it returns the forbidden status + error message
@Injectable()
export class AdminMiddleware implements NestMiddleware {
  use(req: Request, res: Response, next: NextFunction) {
    if (req.session.role !== 'admin') {
      return res
        .status(403)
        .json({ error: 'Forbidden. You are not an admin.' });
    }
    next();
  }
}
