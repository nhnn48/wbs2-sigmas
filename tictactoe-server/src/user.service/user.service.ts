import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { UserDB } from '../database/UserDB';
import { RankName } from '../rankName';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(UserDB)
    private userRepository: Repository<UserDB>,
  ) {}

  getAllUsers(): Promise<UserDB[]> {
    return this.userRepository.find();
  }

  // Creates a user and pushes them into the database
  async createUser(
    firstName: string,
    lastName: string,
    username: string,
    password: string,
  ): Promise<UserDB> {
    const newUser: UserDB = this.userRepository.create();
    const checkUser: UserDB = await this.userRepository.findOne({
      where: { username: username },
    });
    if (checkUser != null && username === checkUser.username) {
      throw new BadRequestException('username already exists');
    }
    newUser.firstName = this.capitalizeFirstLetter(firstName);
    newUser.lastName = this.capitalizeFirstLetter(lastName);
    newUser.username = username;
    newUser.password = password;
    return await this.userRepository.save(newUser);
  }

  // gets a single user from the database by their ID
  async getUserById(id: number): Promise<UserDB> {
    const user = await this.userRepository.findOne({ where: { id } });
    if (!user) {
      throw new NotFoundException('User not found');
    }
    return user;
  }

  // updates a single user in the database
  async updateUser(userData: UserDB): Promise<UserDB> {
    return await this.userRepository.save(userData);
  }

  // only updates the elo value of a single user in the database
  async updateElo(player1: UserDB, player2: UserDB) {
    await this.userRepository.save(player1);
    await this.userRepository.save(player2);
  }

  // deletes the passwort and first/last name of a user, as well as their profile pic from the database
  async deleteUser(id: number): Promise<void> {
    const user = await this.getUserById(id);
    user.password = '';
    user.firstName = '';
    user.lastName = '';
    user.profilePic = 'empty.png';
    await this.userRepository.save(user);
  }

  // gets every user, sorted by their elo values descending
  async getUsersByEloDesc(): Promise<UserDB[]> {
    return this.userRepository.find({ order: { elo: 'DESC' }, take: 10 });
  }

  // gets the two users with slightly higher and lower elo values than the user
  async getUsersBySimilarElo(id: number) {
    const user = await this.getUserById(id);

    // two users with higher elo values
    const higherEloUsers = await this.userRepository
      .createQueryBuilder('user')
      .where('user.elo > :elo', { elo: user.elo })
      .orderBy('user.elo', 'ASC')
      .limit(2)
      .getMany();

    // two users with lower elo values
    const lowerEloUsers = await this.userRepository
      .createQueryBuilder('user')
      .where('user.elo < :elo', { elo: user.elo })
      .orderBy('user.elo', 'DESC')
      .limit(2)
      .getMany();

    return [...higherEloUsers, user, ...lowerEloUsers];
  }

  // gets a specific users position in the leaderboard by their id from the database
  async getRank(id: number): Promise<number> {
    const user = await this.getUserById(id);
    let rank = await this.userRepository
      .createQueryBuilder('user')
      .where('user.elo >= :elo', { elo: user.elo })
      .getCount();
    rank = rank - 2;
    return rank;
  }

  // get the rank assigned to the elo value of a specific user
  getCertainRank(elo: number, state: string): string {
    if (state === 'lowest') {
      return RankName.getRankName(elo - 300);
    }
    if (state === 'lower') {
      return RankName.getRankName(elo - 150);
    }
    if (state === 'higher') {
      return RankName.getRankName(elo + 150);
    }
    if (state === 'highest') {
      return RankName.getRankName(elo + 300);
    }
  }

  // deletes the profile picture of a user and gives them the default one
  async resetProfilePicture(userId: number): Promise<UserDB> {
    const user: UserDB = await this.getUserById(userId);
    user.profilePic = 'empty.png';
    return await this.userRepository.save(user);
  }

  // assigns two user ids together in the friendships table
  async createFriend(userId: number, friendId: number): Promise<UserDB> {
    const user: UserDB = await this.getUserById(userId);
    const friend = await this.userRepository.findOne({
      where: { id: friendId },
    });
    if (!friend) {
      throw new NotFoundException('Friend not found');
    }
    const existingFriend = (await user.friends).find((f) => f.id === friendId);
    if (existingFriend) {
      throw new Error('Friend already exists in the users friend list');
    }
    (await user.friends).push(friend);
    await this.userRepository.save(user);
    return user;
  }

  // gets all friend requests of a user
  async findFriendRequests(userId: number): Promise<UserDB[]> {
    const user: UserDB = await this.getUserById(userId);

    // gets all users
    const allUsers = await this.userRepository.find();

    const friendRequests = await Promise.all(
      allUsers.map(async (otherUser) => {
        if (otherUser.id === userId) {
          return null;
        }

        // checks if the other person has the user as their friend
        const otherUserFriends = await otherUser.friends;
        const hasUserAsFriend = otherUserFriends.some((f) => f.id === userId);
        if (hasUserAsFriend) {
          // checks if the user has the other person as their friend
          const userFriends = await user.friends;
          const hasFriend = userFriends.some((f) => f.id === otherUser.id);
          // if not, returns the user
          if (!hasFriend) {
            return otherUser;
          }
        }
        return null;
      }),
    );
    return friendRequests.filter(Boolean);
  }

  // gets all friends of a user
  async findFriends(userId: number): Promise<UserDB[]> {
    const user: UserDB = await this.getUserById(userId);
    const userFriends = await user.friends;

    // checks if the other person has the user as their friend and if the user as them as their friend
    const mutualFriends = await Promise.all(
      userFriends.map(async (friend) => {
        const friendFriends = await friend.friends;
        const hasUserAsFriend = friendFriends.some((f) => f.id === userId);
        if (hasUserAsFriend) {
          return friend;
        }
        return null;
      }),
    );
    return mutualFriends.filter((friend) => friend !== null);
  }

  // deletes either a friend request, or a friendship from the database
  async deleteFriend(userId: number, friendId: number) {
    const friend = await this.userRepository.findOne({
      where: { id: friendId },
    });
    if (!friend) {
      throw new NotFoundException('Friend not found');
    }
    const userIndex = (await friend.friends).findIndex((u) => u.id === userId);

    if (userIndex !== -1) {
      (await friend.friends).splice(userIndex, 1);
      await this.userRepository.save(friend);

      const user: UserDB = await this.getUserById(userId);
      const index = (await user.friends).findIndex((f) => f.id === friendId);
      if (index === -1) {
        throw new NotFoundException('Friend not found');
      }
      (await user.friends).splice(index, 1);
      await this.userRepository.save(user);
    }
  }

  // gets all users from the database that match the name which is typed in by the user
  async searchByUsername(username: string, userId: number) {
    const allUsers = await this.userRepository.find();
    const user: UserDB = await this.getUserById(userId);
    const otherUsers = await Promise.all(
      allUsers.map(async (otherUser) => {
        if (otherUser.id === user.id) {
          return undefined;
        }
        const otherUserFriends = await otherUser.friends;
        const hasUserAsFriend = otherUserFriends.some((f) => f.id === userId);
        if (!hasUserAsFriend) {
          const userFriends = await user.friends;
          const hasFriend = userFriends.some((f) => f.id === otherUser.id);
          if (!hasFriend) {
            return otherUser;
          }
        }
        return undefined;
      }),
    );
    return allUsers.filter((user) =>
      user.username.toLowerCase().includes(username.toLowerCase()),
    );
  }

  // counts the number of users in the database
  async countUsers(): Promise<number> {
    return await this.userRepository.count();
  }

  // counts the number of friendships in the database
  async countFriendships(): Promise<number> {
    const users = await this.userRepository.find({ relations: ['friends'] });

    let totalCount = 0;
    for (const user of users) {
      const friends = await user.friends;
      totalCount += friends.length;
    }

    return totalCount;
  }

  // gets a random user that exists in the database
  async getRandomUser(): Promise<UserDB> {
    const count: number = await this.countUsers();
    const randomIndex: number = Math.floor(Math.random() * count);
    const user = await this.userRepository
      .createQueryBuilder('user')
      .select()
      .offset(randomIndex)
      .limit(1)
      .getOne();
    if (!user) {
      await this.getRandomUser();
    }
    return user;
  }

  // capitalizes the first letter of a string
  capitalizeFirstLetter(name: string): string {
    const firstLetter: string = name.substring(0, 1).toUpperCase();
    const word: string = name.substring(1, name.length);
    return firstLetter + word;
  }
}
