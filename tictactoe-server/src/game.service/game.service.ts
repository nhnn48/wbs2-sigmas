import { Injectable, NotFoundException } from '@nestjs/common';
import { GameDB } from '../database/GameDB';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { UserDB } from '../database/UserDB';

@Injectable()
export class GameService {
  constructor(
    @InjectRepository(GameDB)
    private gamesRepository: Repository<GameDB>,
  ) {}

  async updateGame(updatedGame: GameDB): Promise<GameDB> {
    // Save the updated game to the database
    return await this.gamesRepository.save(updatedGame).catch((error) => {
      console.error('Error saving game:', error);
      throw error; // Rethrow the error to be caught by the caller HEHEH
    });
  }

  // creates a game that is unfinished
  async createGame(player1: UserDB, player2: UserDB): Promise<GameDB> {
    const rndm: number = Math.floor(Math.random() * 2);
    const game: GameDB = this.gamesRepository.create();
    if (rndm == 0) {
      game.player1 = player1;
      game.player2 = player2;
    } else {
      game.player2 = player1;
      game.player1 = player2;
    }
    game.winner = null;
    return this.gamesRepository.save(game);
  }

  // finds all games
  findAllGames(): Promise<GameDB[]> {
    return this.gamesRepository.find({ relations: ['player1', 'player2'] });
  }

  // find all games that were played by a specific user
  findPlayersGames(id: number): Promise<GameDB[]> {
    const game = this.gamesRepository.find({
      where: [{ player1: { id } }, { player2: { id } }],
      relations: ['player1', 'player2'],
      order: { id: 'DESC' },
    });
    if (game == null) {
      throw new NotFoundException('Game was not found');
    }
    return game;
  }

  // finds a specific game by its id
  findOneGame(id: number): Promise<GameDB> {
    const game = this.gamesRepository.findOneBy({ id });
    if (game == null) {
      throw new NotFoundException('Game was not found');
    }
    return game;
  }

  // finds a game that is currently being played by a specific user
  async findCurGame(id: number): Promise<GameDB> {
    const game = await this.gamesRepository.findOne({
      where: [
        { player1: { id }, finished: false },
        { player2: { id }, finished: false },
      ],
      relations: ['player1', 'player2'],
    });
    if (!game) {
      throw new NotFoundException('Game was not found');
    }
    return game;
  }

  // Finds all games whose status is finished: false in relation to both players
  async findAllCurGames(): Promise<GameDB[]> {
    const games = await this.gamesRepository.find({
      relations: ['player1', 'player2'],

      where: [{ finished: false }],
    });
    if (!games) {
      throw new NotFoundException('Games were not found');
    }
    return games;
  }

  // counts the number of games that are in the database
  async countGames(): Promise<number> {
    return await this.gamesRepository.count();
  }
}
