import { Injectable, OnModuleInit } from '@nestjs/common';
import { UserService } from '../user.service/user.service';
import { GameService } from '../game.service/game.service';
import { faker } from '@faker-js/faker';
import { UserDB } from '../database/UserDB';
import { GameDB } from '../database/GameDB';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class FakerService implements OnModuleInit {
  constructor(
    @InjectRepository(UserDB)
    private userRepository: Repository<UserDB>,
    @InjectRepository(GameDB)
    private gamesRepository: Repository<GameDB>,
    private readonly userService: UserService,
    private readonly gameService: GameService,
  ) {}

  async onModuleInit() {
    await this.addAdmin();
    await this.addUsers();
    await this.addGames();
    await this.addFriends();
  }

  // checks what the number of games in the database is and if there already are too many, doesnt add any
  async addUsers() {
    const userCount: number = await this.userService.countUsers();
    const usersToAdd: number = 100 - userCount;

    if (usersToAdd > 0) {
      for (let i = 0; i < usersToAdd; i++) {
        await this.createUser();
      }
      console.log('users added successfully ');
    } else {
      console.log('database already contains at least 100 users');
    }
  }

  // the names of the default profile pictures
  private readonly fileNames: string[] = [
    '1.gif',
    '2.gif',
    '1.jpg',
    '2.jpg',
    '3.jpg',
    '4.jpg',
    '5.jpeg',
    '1.png',
    '2.png',
    '3.png',
    '4.png',
    '5.png',
    'empty.png',
    '6.png',
    '5.jpg',
  ];

  // chooses a random profile pic for an user
  chooseProfilePic(): string {
    const randomIndex = Math.floor(Math.random() * this.fileNames.length);
    return this.fileNames[randomIndex];
  }

  // creates a user using faker values. checks first if the username is taken
  async createUser(): Promise<UserDB> {
    const newUser: UserDB = this.userRepository.create();
    const username: string = faker.internet.userName();
    const checkUser: UserDB = await this.userRepository.findOne({
      where: { username: username },
    });
    if (checkUser != null && username === checkUser.username) {
      await this.createUser();
    }
    newUser.firstName = faker.person.firstName();
    newUser.lastName = faker.person.lastName();
    newUser.username = username;
    newUser.password = faker.internet.password();
    newUser.elo = faker.number.float({ min: 0, max: 5000 });
    newUser.profilePic = this.chooseProfilePic();
    return await this.userRepository.save(newUser);
  }

  // checks if there already is an admin in the database
  async addAdmin() {
    const checkUser: UserDB = await this.userRepository.findOne({
      where: { username: 'admin' },
    });
    if (checkUser == null) {
      await this.createAdmin();
      console.log('Admin added successfully ');
    } else {
      console.log('there already is an admin in the database');
    }
  }

  // if there is no admin, adds one to the database
  async createAdmin(): Promise<UserDB> {
    const newUser: UserDB = this.userRepository.create();
    newUser.firstName = 'Paul';
    newUser.lastName = 'Paulsen';
    newUser.username = 'admin';
    newUser.password = 'admin';
    newUser.profilePic = 'output.jpg';
    newUser.role = 'admin';
    return await this.userRepository.save(newUser);
  }

  // checks how many friendships are in the database. if there are too many, doesnt add any
  async addFriends() {
    const friendships: number = await this.userService.countFriendships();
    const friendsToAdd: number = 200 - friendships;

    if (friendsToAdd > 0) {
      for (let i = 0; i < friendsToAdd; i++) {
        await this.createFriendship();
      }
      console.log('friendships added successfully ');
    } else {
      console.log('database already contains too many friendships');
    }
  }

  // adds friendships to the database
  async createFriendship() {
    const player1 = await this.userService.getRandomUser();
    let player2 = await this.userService.getRandomUser();

    if (player2.id === player1.id) {
      player2 = await this.userService.getRandomUser();
    }

    // checks if the friendship already exists. if yes, the function is called again
    const existingFriend = (await player1.friends).find(
      (f) => f.id === player2.id,
    );
    if (existingFriend) {
      await this.createFriendship();
    }
    (await player1.friends).push(player2);
    await this.userRepository.save(player1);
  }

  // checks how many games are in the database and adds the correct amount to achieve 100
  async addGames() {
    const gameCount: number = await this.gameService.countGames();
    const gamesToAdd: number = 100 - gameCount;

    if (gamesToAdd > 0) {
      for (let i = 0; i < gamesToAdd; i++) {
        await this.createGame();
      }
      console.log('games added successfully');
    } else {
      console.log('database already contains at least 100 games');
    }
  }

  // creates a game that is then being pushed into the database
  async createGame(): Promise<GameDB> {
    const newGame: GameDB = this.gamesRepository.create();
    const player1 = await this.userService.getRandomUser();
    let player2 = await this.userService.getRandomUser();
    if (player2.id === player1.id) {
      player2 = await this.userService.getRandomUser();
    }
    newGame.player1 = player1;
    newGame.player2 = player2;
    newGame.winner = Math.floor(Math.random() * 3);
    newGame.finished = true;
    return await this.gamesRepository.save(newGame);
  }
}
