

# Willkommen bei unserem Projekt tiktaktUWUroyale!

Um unsere Anwendung richtig zum Laufen zu bringen, empfehlen wir, zunächst unsere Contribution.md Datei durchzulesen und zu befolgen.


## Funktionen unserer Seite


### Einloggen/Registrieren

Nutzer:innen können sich bei uns einen Account erstellen, bei dem sie einen Nutzernamen, ein Passwort und 
ihren Vor-/Nachnamen hinterlegen. 
Der Nutzername muss einzigartig sein und ihnen wird beim Erstellen automatisch die Rolle Nutzer und ein Elo von 1000 gegeben.

### Profil

In ihrem Profil haben Nutzer:innen die Möglichkeit ein eigenes Profilbild hochzuladen, welches daraufhin auch von 
anderen Nutzer:innen gesehen werden kann. Dieses können sie auch wieder löschen.
Außerdem können sie hier ihre Daten einsehen, ihr Passwort ändern und auch ihren Account löschen. 
Ausloggen können sie sich über das Profilicon im Header.

### Spiel

Auf der Startseite können die Nutzer:innen ein Spiel starten und werden automatisch in eine Warteschlange hinzugefügt, 
bis ein andere/r Nutzer:in mit ähnlichem Elo die Warteschlange betritt.
Währenddessen können die Nutzer:innen anhand eines Timers sehen, wie lange sie sich schon in der Warteschlange befinden.
Falls der/die Nutzer:in zu viele Versuche/zu lange braucht, wird der erlaubte Elo-Unterschied langsam verringert. 
Sobald ein/e andere/r Spieler:in gefunden wurde wird per Zufall entschieden, wer anfängt.


Die Spieler:innen spielen nun gegeneinander ein klassisches Tiktaktoe-Spiel. Im Spiel können sie ihr Gegenüber 
mitsamt Nutzernamen, Profilbild und Elo sehen.
Im Anschluss wird je nach Gewinner:in berechnet, was der neue Elo-Wert der Spieler:innen ist.
Ihnen wird das Angebot gemacht erneut zu spielen und die Queue zu betreten, oder das Spiel zu verlassen.

### Match-History

Spieler:innen können auf der Match-History Seite einsehen, was ihre letzten Spiele waren, gegen wen sie spielten und wie diese ausgingen. 
Zudem haben sie hier eine Statistik, wie viele Spiele sie gewonnen und verloren haben und wie viele unentschieden ausgegangen sind.

### Leaderboard/Rang

Auf der Leaderboardseite können Nutzer:innen die 10 Spieler:innen sehen, die aktuell die höchsten Elo-Werte haben. 
Außerdem sehen sie ihre eigene Position und die 2 Spieler, die jeweils über und unter ihnen im Ranking sind.
Auf der Rangseite können Nutzer:innen ihren aktuellen Rang sehen, der zu ihrem Elo-Wert gehört, und die beiden Ränge darunter und darüber.

### Freundesliste

Nutzer:innen haben die Möglichkleit, andere registrierte Mitspieler:innen zu suchen und ihnen eine Freundschaftsanfrage zu schicken.
Der/die Mitspieler:in kann diese Anfrage annehmen oder ablehnen. 
Sobald eine Freunschaft geschlossen ist, werden die Nutzer:innen gegenseitig in ihren Freundeslisten angezeigt. 
Man kann nun gezielt gegeneinander Spielen, oder die andere Person wieder aus der Liste entfernen. 
Beim Senden einer Spielanfrage wird dem/der anderen Spieler:in die Möglichkeit gegeben, die Anfrage anzunehmen oder abzulehnen. 
Wird sie angenommen, wird automatisch ein Spiel der beiden kreiert. Wird sie abgelehnt, wird der anderen Person eine Nachricht geschickt.

### Admin

Der Admin hat die Möglichkeit, aktuell laufende Spiele und die Mitspieler:innen auf seinem Dashboard einzusehen. 
Er kann zudem sehen, wer sich momentan in der Warteschlange befindet, und hat eine Übersicht über alle Nutzer:innen mit ihren Daten.

## Struktur

### Frontend

Die Dateien für das Frontend unserer Anwendung findet man im Ordner tictactoe-client.
Im Ordner src ist der Ordner media zu finden, in dem Hintergrundbilder, Icons und andere Assets abgelegt sind.
Zudem findet man in src den Ordner service, in dem einige übergreifende Funktionen für den Client gespeichert sind.
Der Header fungiert als Router, der die einzelnen Seiten miteinander verknüpft. 
Im Ordner Pages findet man alle Seiten, die auf der Website erreichbar sind. Sie sind in die drei Bereiche admin, game und profile gegliedert, die wiederum ihre Unterseiten in Ordner aufteilen.

### Backend

Die Dateien für das Backend unserer Anwendung findet man im Ordner tictactoe-server. 
Im Uploads Ordner befinden sich sämtliche Daten, die von Nutzer:innen hochgeladen werden, sowie die default-Profilbilder für generierte Nutzer:innen.
Der src Ordner umfasst alle Services und Controller der Anwendung, die auch dementsprechend benannt sind, sowie die genutzten DTOs.
In der handleSockets Datei werden die Websockets verwaltet.
Im Ordner database sind die Dateien hinterlegt, die zum Generieren der Datenbank benötigt werden und die Struktur vorgeben.

## Technologien

Bei der Entwicklung unseres Projekts haben wir das Frontend-Framework React genutzt und React-Bootstrap verwendet. 
Als Backend-Framework wurde Nestjs verwendet, und die Daten werden per TypeORM in einer neu generierten SQLite Datei gespeichert. Zum Generieren der Demo-Daten wurde Faker genutzt. 
Zudem wurde OpenAPI verwendet, um Server-Routen zu testen und zu dokumentieren. 
