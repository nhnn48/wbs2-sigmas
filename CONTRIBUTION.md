
# Wichtige Hinweise zum Starten des Projekts

## Installation

Zum Verwenden unserer Anwendung müssen zunächst alle Dependencies vollständig installiert werden.
Hierfür müssen die Befehle `npm i` in den Directories tictactoe-client und tictactoe-server ausgeführt werden.

## Start

Um den Server zum Laufen zu bringen, muss der Befehl `npm run start` im directory tictactoe-server ausgeführt werden. 
Das Frontend lässt sich durch `npm run build` im Directory tictactoe-client builden.

Erreichen kann man die Seite über http://localhost:3000. Über http://localhost:3000/api lässt sich die Api Dokumentation aufrufen.

Beim Start des Servers werden automatisch Nutzer, Spiele und Freundschaften generiert. Um sich als Admin anzumelden, werden folgende Daten benötigt:

#### Nutzername: admin / Passwort: admin

Viel Spaß auf unserer Seite wünschen Alex, Annalena, Nico und Tina :)
